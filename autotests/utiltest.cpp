/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTest>
#include <QDebug>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ksandbox/util.h"


class UtilTest : public QObject {
    Q_OBJECT
public:
    QString exceptionMessage(std::function<void()> func) {
        try {
            func();
        } catch (KSandbox::Exception& e) {
            return e.message();
        }
        return QString();
    }
private Q_SLOTS:
    void testSyscall() {
        const char* badPath = "/foo/bar/doesn/t/exist";
        QCOMPARE(exceptionMessage([&](){ (void)KSANDBOX_SYSCALL(open(badPath, 0), DBG_VAR(badPath)); }),
                 QStringLiteral("open(badPath, 0) failed: No such file or directory. badPath = /foo/bar/doesn/t/exist"));
        qDebug() << "Checking noexception version";
        KSANDBOX_SYSCALL_MESSAGE_ON_ERROR(qWarning(), open(badPath, 0), " Failed to open file ", badPath);

    }

    void testRequire() {
        int fd = 1;
        QCOMPARE(exceptionMessage([&](){ KSANDBOX_REQUIRE(true, DBG_VAR(fd)); }), QString());
        QCOMPARE(exceptionMessage([&](){ KSANDBOX_REQUIRE(false, DBG_VAR(fd)); }),
                 QStringLiteral("Requirement \"false\" failed. fd = 1"));
        void* foo = nullptr;
        QCOMPARE(exceptionMessage([&](){ KSANDBOX_REQUIRE_NONNULL(foo); }),
                 QStringLiteral("Requirement \"foo != nullptr\" failed."));

        void* bar = (void*)0x1234;
        QCOMPARE(exceptionMessage([&](){ KSANDBOX_REQUIRE_EQUAL(foo, bar); }),
                 QStringLiteral("Requirement \"foo == bar\" failed. foo = 0x0 bar = 0x1234"));
    }
};

QTEST_MAIN(UtilTest)

#include "utiltest.moc"
