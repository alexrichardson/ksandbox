/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTest>
#include <QDebug>
#include <qthread.h>

#include "ksandbox/process.h"
#include "ksandbox/util.h"
#include <unistd.h>
#include <signal.h>


class ProcessTest : public QObject {
    Q_OBJECT
private Q_SLOTS:

    void initTestCase() {
        //qputenv("KSANDBOX_NO_PROCDESC", "1");
    }

    void testCurrentProcess() {
        QVERIFY(KSandbox::Process::self().isAlive());
    }

    void testFork() {
        KSandbox::Process p1 = KSandbox::Process::fork([]() {
            auto& proc = KSandbox::Process::self();
            qDebug("Forked %d", proc.pid());
            KSANDBOX_REQUIRE_EQUAL(proc.pid(), getpid());
            KSANDBOX_REQUIRE_EQUAL(proc.processDescriptor(), -1);
            KSANDBOX_REQUIRE(KSandbox::IO::isSocketFd(proc.rpcSocket()), DBG_VAR(proc.rpcSocket().fd));
            KSANDBOX_REQUIRE(KSandbox::IO::isSocketFd(proc.fdTransferSocket()), DBG_VAR(proc.fdTransferSocket().fd));
            int buf = 1;
            KSANDBOX_SYSCALL(write(proc.rpcSocket().fd, &buf, sizeof(buf)), DBG_VAR(proc.rpcSocket().fd));
            KSANDBOX_SYSCALL(read(proc.rpcSocket().fd, &buf, sizeof(buf)), DBG_VAR(proc.rpcSocket().fd));
            return 4;
        });
        qDebug("Parent %d, child (%d)", getpid(), p1.pid());
        KSANDBOX_REQUIRE(KSandbox::IO::isSocketFd(p1.rpcSocket()));
        KSANDBOX_REQUIRE(KSandbox::IO::isSocketFd(p1.fdTransferSocket()));
        QVERIFY(p1.pid() >= 0);
        // TODO: require process descriptors
        qDebug("process descriptor for child %d is %d", (int)p1.pid(), p1.processDescriptor());
        // QVERIFY(p1.processDescriptor() >= 0);
        int buf = 0;
        KSANDBOX_SYSCALL(read(p1.rpcSocket().fd, &buf, sizeof(buf))); // wait for ready
        QCOMPARE(buf, 1);
        QVERIFY(p1.isAlive());
        KSANDBOX_SYSCALL(write(p1.rpcSocket().fd, &buf, sizeof(buf))); // tell it to exit
        int status;
        QVERIFY(p1.wait(&status));
        QVERIFY(!p1.isAlive());
        qDebug("exit code = %d, term sig = %d", WEXITSTATUS(status), WTERMSIG(status));
        QVERIFY(!WIFSIGNALED(status));
        QVERIFY(WIFEXITED(status));
        QCOMPARE(WEXITSTATUS(status), 4);
    }

    void testSpawn() {
        KSandbox::Process p1 = KSandbox::Process::spawn(QStringList() << "echo" << "foo" << "bar", []() {
            auto& proc = KSandbox::Process::self();
            qDebug("Forked %d", proc.pid());
            Q_ASSERT(proc.pid() == getpid());
            Q_ASSERT(proc.processDescriptor() == -1);
            Q_ASSERT(KSandbox::IO::isSocketFd(proc.rpcSocket()));
            Q_ASSERT(KSandbox::IO::isSocketFd(proc.fdTransferSocket()));
            // now make sure echo writes to the fd transfer rpcSocket
            dup2(proc.rpcSocket().fd, STDOUT_FILENO);
        });
        qDebug("Parent %d, child %d", getpid(), p1.pid());
        QVERIFY(KSandbox::IO::isSocketFd(p1.rpcSocket()));
        QVERIFY(KSandbox::IO::isSocketFd(p1.fdTransferSocket()));
        QVERIFY(p1.pid() >= 0);
        // TODO: require process descriptors
        // QVERIFY(p1.processDescriptor() >= 0);
        qDebug("Waiting for %d to exit", p1.pid());
        // wait for exit

        int status;
        QVERIFY(p1.wait(&status));
        qDebug("%d exited", p1.pid());
        QVERIFY(!p1.isAlive());
        QVERIFY(WIFEXITED(status));
        QCOMPARE(WEXITSTATUS(status), 0);

        char buf[1024] = {};
        QVERIFY(read(p1.rpcSocket().fd, buf, sizeof(buf)) >= 0);
        QCOMPARE(buf, "foo bar\n");
    }


    void testKill() {
        KSandbox::Process p1 = KSandbox::Process::spawn(QStringList() << "sleep" << "100");
        QThread::msleep(200); // make sure it starts so that the signal is not delivered before exec()
        QVERIFY(p1.isAlive());
        qDebug("Parent %d, child %d", getpid(), p1.pid());
        QCOMPARE(kill(p1.pid(), SIGHUP), 0);
        QThread::msleep(1000); // race?
        int status;
        QVERIFY(p1.wait(&status));
        qDebug("status = %x", status);
        QVERIFY(!p1.isAlive());
        QVERIFY(!WIFEXITED(status));
        QVERIFY(WIFSIGNALED(status));
        QCOMPARE(WTERMSIG(status), SIGHUP);
    }

    void testChildIsKilledOnParentExit() {
        KSandbox::Process p1 = KSandbox::Process::fork([]() {
            qDebug("child %d", getpid());
            KSandbox::Process p2 = KSandbox::Process::fork([]() {
                qDebug("grandchild %d", getpid());
                signal(SIGTERM, [](int s) {
                    qDebug("received SIGTERM (%d) -> exit", s);
                    exit(1);
                });
                QThread::sleep(10);
                return 0;
            });
            pid_t buf = p2.pid();
            auto& proc = KSandbox::Process::self();
            write(proc.rpcSocket().fd, &buf, sizeof(buf));
            read(proc.rpcSocket().fd, &buf, sizeof(buf));
            qDebug("child %d exiting!", getpid());
            return 0;
        });
        QThread::msleep(200); // make sure it starts so that the signal is not delivered before exec()
        QVERIFY(p1.isAlive());
        pid_t grandchild = 0;
        read(p1.rpcSocket().fd, &grandchild, sizeof(grandchild)); // wait for ready
        qDebug("Parent %d, child %d grandchild %d", getpid(), p1.pid(), grandchild);
        write(p1.rpcSocket().fd, &grandchild, sizeof(grandchild)); // tell it to exit
        QCOMPARE(kill(grandchild, 0), 0); // alive
        int status;
        QVERIFY(p1.wait(&status));
        errno = 0;
        QCOMPARE(kill(p1.pid(), 0), -1); // dead
        QCOMPARE(errno, ESRCH); // pid gone
        // same for grandchild
        QThread::msleep(200); // wait for kernel to kill it
        errno = 0;
        QCOMPARE(kill(grandchild, 0), -1); // also dead
        QCOMPARE(errno, ESRCH); // pid gone

    }
};

QTEST_APPLESS_MAIN(ProcessTest)

#include "processtest.moc"
