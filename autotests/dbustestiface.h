/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <QObject>
#include <QDebug>
#include <QDBusContext>
#include <qdbusunixfiledescriptor.h>
#include <QCoreApplication>
#include <QVector>

#include <unistd.h>
#include "ksandbox/util.h"

struct Element {
    QString name;
    uint32_t id;
};

struct Image {
    uint32_t height;
    uint32_t width;
    QByteArray data;
};

class TestIfaceImpl : public QObject, protected QDBusContext {
    Q_OBJECT
public:
    TestIfaceImpl(QObject* parent);
    virtual ~TestIfaceImpl();
public Q_SLOTS:
    void quit(uint code) {
        qDebug() << "received quit msg:" << code << message();
        QCoreApplication::exit(code);
    }
    QString echo(const QString& f) {
        qDebug() << "received echo msg:" << f << message();
        return f;
    }
    QString testFD(const QDBusUnixFileDescriptor &fd) {
        qDebug() << "received fd msg:" << fd.fileDescriptor() << message();
        int value;
        KSANDBOX_SYSCALL(::read(fd.fileDescriptor(), &value, sizeof(value)));
        return QString::number(value);

    }

//     QVector<Element> getElements(uint8_t count);
//     QVector<uint> getList();
//     QVector<Image> getStructList();
//     int32_t runInThread(uint timeout);
};
