/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTest>
#include <QDebug>
#include <QDBusArgument>
#include <QDBusConnection>
#include <QDBusMetaType>
#include <QDBusVirtualObject>
#include <QJsonDocument>
#include <KCrash/KCrash>

#include <memory>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ksandbox/util.h"
#include "ksandbox/process.h"
#include "ksandbox/dbusutils.h"
#include "ksandbox/serialization.h"

#include "dbustestiface.h"
#include "testifaceproxy.h"
#include "dbustest_config.h"


//TODO: test
extern QString qDBusGenerateMetaObjectXml(QString interface, const QMetaObject *mo, const QMetaObject *base, int flags);

//static inline QString dbusPath(KSandbox::FileDescriptor file) {
//    return QStringLiteral("unix:tmpdir=/tmp");
//}


struct CustomDBusType {
    int i;
    QString str;
    QDBusUnixFileDescriptor fd;
};

Q_DECLARE_METATYPE(CustomDBusType)
// Marshall the CustomDBusType data into a D-Bus argument
QDBusArgument &operator<<(QDBusArgument &argument, const CustomDBusType &mystruct)
{
    argument.beginStructure();
    argument << mystruct.i << mystruct.str << mystruct.fd;
    argument.endStructure();
    return argument;
}

// Retrieve the MyStructure data from the D-Bus argument
const QDBusArgument &operator>>(const QDBusArgument &argument, CustomDBusType &mystruct)
{
    argument.beginStructure();
    argument >> mystruct.i >> mystruct.str >> mystruct.fd;
    argument.endStructure();
    return argument;
}

// TODO: apparently leading or trailing stuff is not allowed in QtDBus
// (asii(ss)ia{s(ish)})
struct ComplexDBusType {

    // begin struct
    // begin array
    QString s1 = "s1";
    // end array
    int i1 = 1;
    int i2 = 2;
    // begin struct
    QString s2 = "s2";
    QString s3 = "s3";
    // end struct
    int i3 = 3;
    // map
    QMap<QString, QRect> map;
    QList<QSize> list;
    // end map
    QString s4 = "s4";
    //end struct
};

Q_DECLARE_METATYPE(ComplexDBusType)
QDBusArgument &operator<<(QDBusArgument &argument, const ComplexDBusType& c)
{
    argument.beginStructure();
    argument.beginArray(qMetaTypeId<QString>());
    argument << c.s1;
    argument.endArray();
    argument << c.i1 << c.i2;
    argument.beginStructure();
    argument << c.s2 << c.s3;
    argument.endStructure();
    argument << c.i3;
    argument.beginMap(qMetaTypeId<QString>(), qMetaTypeId<QRect>());
    for (auto it = c.map.begin(); it != c.map.end(); ++it) {
        argument.beginMapEntry();
        argument << it.key() << it.value();
        argument.endMapEntry();
    }
    argument.endMap();
    argument << c.list; // list
    argument << c.s4;
    argument.endStructure();
    return argument;
}
const QDBusArgument &operator>>(const QDBusArgument &argument, ComplexDBusType& c)
{
    argument.beginStructure();
    argument.beginArray();
    argument >> c.s1;
    argument.endArray();
    argument >> c.i1 >> c.i2;
    argument.beginStructure();
    argument >> c.s2 >> c.s3;
    argument.endStructure();
    argument >> c.i3;
    argument.beginMap();
    while (!argument.atEnd()) {
        QString key;
        QRect value;
        argument.beginMapEntry();
        argument >> key >> value;
        argument.endMapEntry();
        c.map.insert(key, value);
    }
    argument.endMap();
    argument >> c.list;
    argument >> c.s4;
    argument.endStructure();
    return argument;
}


// this one manual:
struct SimpleWrappingManual {
    int first;
    int second;

private:
    inline auto ksandbox_asTuple() const -> decltype(std::tie(first, second))
    {
        return std::tie(first, second);
    }

    inline auto ksandbox_asMutableTuple() -> decltype(std::tie(first, second))
    {
        return std::tie(first, second);
    }
    friend QDBusArgument& operator<<(QDBusArgument&, const SimpleWrappingManual&);
    friend const QDBusArgument& operator>>(const QDBusArgument&, SimpleWrappingManual&);
};


Q_DECLARE_METATYPE(SimpleWrappingManual)
QDBusArgument& operator<<(QDBusArgument& argument, const SimpleWrappingManual& s) {
    const auto tuple = s.ksandbox_asTuple();
    argument.beginStructure();
    argument << std::get<0>(tuple);
    argument << std::get<1>(tuple);
    argument.endStructure();
    return argument;
}
const QDBusArgument &operator>>(const QDBusArgument &argument, SimpleWrappingManual& s) {
    const auto tuple = s.ksandbox_asMutableTuple();
    argument.beginStructure();
    argument >> std::get<0>(tuple);
    argument >> std::get<1>(tuple);
    argument.endStructure();
    return argument;
}
static const int SimpleWrapping1_MetaType = qDBusRegisterMetaType<SimpleWrappingManual>();


struct SimpleWrappingAutomated {
    int first;
    int second;
    KSANDBOX_SERIALIZED_MEMBERS(SimpleWrappingAutomated, (first, second))
};

KSANDBOX_DECLARE_SERIALIZABLE_CLASS(SimpleWrappingAutomated);
KSANDBOX_SERIALIZABLE_CLASS_DEFINITION(SimpleWrappingAutomated)

struct ComplexWrappingAutomated {
    int i;
    QString str;
    QMap<QString, int> map;
    QRect rect;
    QVector<int> vector;
    std::vector<int> stdVector;
    std::list<int> stdList;
    std::deque<int> stdDeque;
    std::array<int, 2> stdArray;
    QVarLengthArray<int, 4> qvarLenghArray;
    int end;
    KSANDBOX_SERIALIZED_MEMBERS(ComplexWrappingAutomated, (i, str, map, rect, vector, stdVector, stdList, stdDeque, stdArray, qvarLenghArray, end))
};

KSANDBOX_DECLARE_SERIALIZABLE_CLASS(ComplexWrappingAutomated);
KSANDBOX_SERIALIZABLE_CLASS_DEFINITION(ComplexWrappingAutomated)

class DBusTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void initTestCase() {
        KCrash::setCrashHandler();
        KCrash::setDrKonqiEnabled(true);
        devZeroFd = KSANDBOX_SYSCALL(::open("/dev/zero", O_RDONLY));
        QThread::currentThread()->setObjectName(QLatin1String("main thread"));
        QVERIFY(!rpc.server().isConnected());
        QCOMPARE(rpc.server().name(), QString());
        // We can't use fork here because that does not interact well with QDBusConnection (it deadlocks if some stuff has already been initialized)
#if 0
        proc = KSandbox::Process::fork([this]() {
            qputenv("LD_PRELOAD", "/home/alex/devel/dbus/cmake/build/lib/libdbus-1.so.3.16.4");
            qputenv("DBUS_VERBOSE", "1");
            rpc = KSandbox::DBus::Connection::connectToParent();
            auto adaptor = new TestIfaceAdaptor(new TestIfaceImpl(QCoreApplication::instance()));
            KSANDBOX_REQUIRE(rpc.exportObject("/dbustest", adaptor));
            qDebug() << "child client object:" << rpc.server().objectRegisteredAt("/dbustest");
            KSANDBOX_REQUIRE_NONNULL(rpc.server().objectRegisteredAt("/dbustest"));
            KSANDBOX_REQUIRE((rpc.client().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
            KSANDBOX_REQUIRE((rpc.server().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
            KSANDBOX_REQUIRE(rpc.server().registerVirtualObject("/virtual", new DBusEchoVirtualObject));

//             auto msg = QDBusMessage::createMethodCall(QString(), QStringLiteral("/dummy"), QString(), QString("__noop"));
//             QDBusMessage response = dbusConnection.call(msg);
//             qDebug() << "parent reply to" << msg << "was" << response;
            // ! QCoreApplication already exists since we forked
            return QCoreApplication::instance()->exec();
        });
#endif
        proc = KSandbox::Process::spawn(QStringList() << QStringLiteral(CHILD_EXECUTABLE));
        qDebug("Parent %d, child (%d)", getpid(), proc.pid());
        rpc = KSandbox::DBus::Connection::connectToChild(proc);
        QVERIFY(rpc.server().isConnected());
        QVERIFY(rpc.client().isConnected());
        // send a dummy message
        // service is optional in p2p connection
        // interface also not needed AFAIK
        auto msg = QDBusMessage::createMethodCall(QString(), QStringLiteral("/dummy"), QString(), QStringLiteral("__noop"));
        QDBusMessage response = rpc.client().call(msg);
        qDebug() << "parent reply to" << msg << "was" << response;
        QVERIFY((rpc.client().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
        QVERIFY((rpc.server().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
        client.reset(new TestIfaceProxy(QString(), "/dbustest", rpc.client()));
        auto echoResult = client->echo("Hello, World!");
        echoResult.waitForFinished();
        QCOMPARE(echoResult.value(), QStringLiteral("Hello, World!"));
        // TODO: blocking calls can deadlock if both child and parent do so at the same time? use two separate dbusconnections? or require always async?
    }

    void testMetaType()
    {
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<QList<int>>()), "ai");
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<QDBusUnixFileDescriptor>()), "h");
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<QRect>()), "(iiii)");
        //qDBusRegisterMetaType<Foo>();
        // QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<Foo>()), "(ih)");
    }

    void testSendFd()
    {
        auto reply = client->testFD(QDBusUnixFileDescriptor(devZeroFd));
        reply.waitForFinished();
        QCOMPARE(reply.value(), QStringLiteral("0"));
    }

    void testSendTypes()
    {
        QVariantMap map{ {"a", 123}, {"b", "str"}};

        auto msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call1");
        msg << map;
        QDBusMessage response = rpc.client().call(msg);
        qDebug() << "response" << response.arguments();
        qDebug() << "q" << qdbus_cast<QVariantMap>(response.arguments().at(0));
        qDebug() << "k" << KSandbox::DBus::toQVariant(response);
        QCOMPARE(KSandbox::DBus::toQVariant(response), QVariant(map));
        qDebug() << response;

        // This works -> no more need for XML parsing in the parent!
        QVariantMap nested{ { "c", 100.123 }, { "map", map } };
        msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call2");
        msg << nested;
        response = rpc.client().call(msg);
        qDebug() << "q" << qdbus_cast<QVariantMap>(response.arguments().at(0));
        qDebug() << "k" << KSandbox::DBus::toQVariant(response);
        QCOMPARE(KSandbox::DBus::toQVariant(response), QVariant(nested));

        // now try some custom QDBusArgument stuff

        msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call3");
        // not registered yet -> should be an error
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<CustomDBusType>()), (const char*)nullptr);
        CustomDBusType custom;
        custom.i = 100;
        custom.str = "asd";
        msg << QVariant::fromValue(custom);
        response = rpc.client().call(msg);
        qDebug() << response.arguments() << response.errorMessage();
        QCOMPARE(response.errorMessage(), QStringLiteral("Marshalling failed: Unregistered type CustomDBusType passed in arguments"));

        // Now it should work:
        qDBusRegisterMetaType<CustomDBusType>();
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<CustomDBusType>()), "(ish)");

        msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call4");
        msg << QVariant::fromValue(custom);
        response = rpc.client().call(msg);
        qDebug() << response.arguments() << response.errorMessage();
        QCOMPARE(response.errorMessage(), QStringLiteral("Marshalling failed: Invalid file descriptor passed in arguments"));

        msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call5");
        // now make sure the fd is valid
        custom.fd.setFileDescriptor(devZeroFd);
        msg << QVariant::fromValue(custom);
        response = rpc.client().call(msg);
        qDebug() << response.arguments() << response.errorMessage();
        QCOMPARE(response.errorMessage(), QString());
        {
            QVariantList asVariantList = KSandbox::DBus::toQVariant(response).toList();
            qDebug() << "k" << asVariantList;
            QCOMPARE(asVariantList.size(), 3);
            QCOMPARE(asVariantList[0].toInt(), custom.i);
            QCOMPARE(asVariantList[1].toString(), custom.str);
            // can't compare the FD
        }
        // qDebug() << "k" << KSandbox::DBus::toQVariant(response);
        // QCOMPARE(KSandbox::DBus::toQVariant(response), QVariant(map));
    }

    void testComplexType()
    {
        // must already be registered
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<CustomDBusType>()), "(ish)");
        ComplexDBusType data;

        auto msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call3");
        // not registered yet -> should be an error
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<ComplexDBusType>()), (const char*)nullptr);
        msg.setArguments(QVariantList() << QVariant::fromValue(data));
        auto response = rpc.client().call(msg);
        qDebug() << response.arguments() << response.errorMessage();
        QCOMPARE(response.errorMessage(), QStringLiteral("Marshalling failed: Unregistered type ComplexDBusType passed in arguments"));

        // Now it should work:
        qDBusRegisterMetaType<ComplexDBusType>();

        // QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<ComplexDBusType>()), "s(ii(ss)i)s");
        QCOMPARE(QDBusMetaType::typeToSignature(qMetaTypeId<ComplexDBusType>()), "(asii(ss)ia{s(iiii)}a(ii)s)");

        data.map.insert("custom", QRect{1, 2, 3, 4});
        data.list << QSize(10, 20) << QSize(42, 42);
        msg.setArguments(QVariantList() << QVariant::fromValue(data));
        response = rpc.client().call(msg);
        qDebug() << response.arguments() << response.errorMessage();
        auto result = KSandbox::DBus::toQVariant(response);
        qDebug() << "converted to variant:" << result;
        auto expected = QVariant{
            QVariantList{ // "("
                QVariantList{"s1"}, // "as"
                1, 2, QVariantList{"s2", "s3"}, 3, // "ii(ss)i"
                // using a QRect here won't work -> use QVariantList
                QVariantMap{ {"custom", QVariantList{1, 2, 3, 4} } }, //  "a{s(iiii)}"
                QVariantList{ QVariantList{10, 20}, QVariantList{42, 42} }, // "a(ii)"
                "s4" // "s"
            } // ")"
        };
        qDebug() << "expected" << QJsonDocument::fromVariant(expected).toJson(QJsonDocument::Compact);
        qDebug() << "result  " << QJsonDocument::fromVariant(result).toJson(QJsonDocument::Compact);

        // TODO: low-priority: also test map with a non-string key...
        QCOMPARE(result, expected);

    }

    void testSimpleWrapping()
    {
        {
            SimpleWrappingManual wrapInput { 10, 20 };
            auto msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call");
            msg << QVariant::fromValue(wrapInput);
            qDebug() << "msg:" << msg <<  "arg:" << QVariant::fromValue(wrapInput);
            QDBusMessage response = rpc.client().call(msg);
            qDebug() << "response" << response << "args:" << response.arguments();
            auto wrapResult = qdbus_cast<SimpleWrappingManual>(response.arguments().at(0));
            QCOMPARE(wrapResult.first, 10);
            QCOMPARE(wrapResult.second, 20);
            QCOMPARE(wrapResult.first, wrapInput.first);
            QCOMPARE(wrapResult.second, wrapInput.second);
        }

        // now make sure that the macros do the same thing
        {
            SimpleWrappingAutomated wrapInput { 30, 40 };
            auto msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call");
            msg << QVariant::fromValue(wrapInput);
            qDebug() << "msg:" << msg <<  "arg:" << QVariant::fromValue(wrapInput);
            QDBusMessage response = rpc.client().call(msg);
            qDebug() << "response" << response << "args:" << response.arguments();
            auto wrapResult = qdbus_cast<SimpleWrappingAutomated>(response.arguments().at(0));
            QCOMPARE(wrapResult.first, 30);
            QCOMPARE(wrapResult.second, 40);
            QCOMPARE(wrapResult.first, wrapInput.first);
            QCOMPARE(wrapResult.second, wrapInput.second);
        }
        {
            ComplexWrappingAutomated wrapInput;
            wrapInput.i = 42;
            wrapInput.str = QStringLiteral("foo");
            auto simpleMap = QMap<QString, int>{
                { QStringLiteral("one"), 1 },
                { QStringLiteral("two"), 2 }
            };
            wrapInput.map = simpleMap;
            wrapInput.rect = QRect(0, 1, 2, 3);
            wrapInput.vector = QVector<int>{1, 2, 3, 4};
            wrapInput.stdVector = std::vector<int>{1, 2, 3};
            wrapInput.stdList = {10, 20, 30};
            wrapInput.stdDeque = {100, 200, 300};
            wrapInput.stdArray = { {42, 24} };
            wrapInput.qvarLenghArray = {3, 2, 1};
            wrapInput.end = 1;
            auto msg = QDBusMessage::createMethodCall(QString(), "/virtual", QString(), "call");
            msg << QVariant::fromValue(wrapInput);
            qDebug() << "msg:" << msg <<  "arg:" << QVariant::fromValue(wrapInput);
            QDBusMessage response = rpc.client().call(msg);
            qDebug() << "response" << response << "args:" << response.arguments();
            auto wrapResult = qdbus_cast<ComplexWrappingAutomated>(response.arguments().at(0));
            QCOMPARE(wrapResult.i, 42);
            QCOMPARE(wrapResult.str, QStringLiteral("foo"));
            QCOMPARE(wrapResult.map, simpleMap);
            QCOMPARE(wrapResult.rect, QRect(0, 1, 2, 3));
            QCOMPARE(wrapResult.vector, QVector<int>({1, 2, 3, 4}));
            QCOMPARE(wrapResult.stdVector, std::vector<int>({1, 2, 3}));
            QCOMPARE(wrapResult.stdList, std::list<int>({10, 20, 30}));
            QCOMPARE(wrapResult.stdDeque, std::deque<int>({100,200,300}));
            QCOMPARE(wrapResult.stdArray, (std::array<int, 2>({{42, 24}})));
            QCOMPARE(wrapResult.qvarLenghArray, (QVarLengthArray<int, 4>({3, 2, 1})));
            QCOMPARE(wrapResult.end, 1);

        }
    }

    void cleanupTestCase() {
        auto quitResult = client->quit(3);
        // client->quit returns immediately
        quitResult.waitForFinished(); // now exit will certainly have been called
        int status;
        proc.wait(&status);
        qDebug() << "Exit status:" << QString::number(status, 16);
        QCOMPARE(WTERMSIG(status), 0);
        QCOMPARE(WEXITSTATUS(status), 3);
        ::close(devZeroFd);
    }

private:
    KSandbox::Process proc;
    KSandbox::DBus::Connection rpc;
    std::unique_ptr<TestIfaceProxy> client;
    int devZeroFd = -1;
};

QTEST_MAIN(DBusTest)

#include "dbustest.moc"
