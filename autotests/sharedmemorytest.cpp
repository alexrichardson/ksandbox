/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTest>
#include <QDebug>
#include <qthread.h>

#include "ksandbox/process.h"
#include "ksandbox/util.h"
#include "ksandbox/guiutils.h"
#include <unistd.h>
#include <signal.h>


class SharedMemoryTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testCurrentProcess() {
        QVERIFY(KSandbox::Process::self().isAlive());
    }

    void testBasicImageFromShm() {
        KSandbox::SharedMemory mem(size_t(4096));
        auto memfd = mem.handle();
        QVERIFY2(KSandbox::IO::isValidFd(memfd), QByteArray::number(memfd.fd).data());
        QCOMPARE(mem.totalSize(), 4096ul);
        QCOMPARE(mem.mappedSize(), 0ul);
        mem.map(4096);
        auto imageSize = 20 * 20 * 4;
        QCOMPARE(mem.mappedSize(), 4096ul);
        {
            QImage extendLifetime;
            QCOMPARE(extendLifetime.byteCount(), 0);
            {
                auto memBegin = mem.begin();
                QImage image = KSandbox::imageFromSharedMemory(std::move(mem), 20, 20, QImage::Format_RGB32);
                QVERIFY(!image.isNull());
                QCOMPARE(image.bits(), memBegin);
                QCOMPARE(image.byteCount(), imageSize);
                // now mem should be invalid since the QImage owns it
                QCOMPARE(mem.handle().fd, -1);
                QCOMPARE(mem.mappedSize(), 0ul);
                QCOMPARE(mem.totalSize(), 0ul);
                QCOMPARE(mem.begin(), (uchar*)nullptr);
                // but the fd should still be valid
                QVERIFY(KSandbox::IO::isValidFd(memfd));
                extendLifetime = image;
                QCOMPARE(extendLifetime.byteCount(), imageSize);
                qDebug("Exiting first scope");
            }
            qDebug("Exited first scope");
            // although the original image was closed it should still remain valid
            QVERIFY(KSandbox::IO::isValidFd(memfd));
            QCOMPARE(extendLifetime.byteCount(), imageSize);
            qDebug("Exiting second scope");
        }
        qDebug("Exited second scope");
        // image exists scope
        QVERIFY(!KSandbox::IO::isValidFd(memfd));

    }
    void testNoExplicitMMapRequired() {
        KSandbox::SharedMemory mem(size_t(4096));
        auto memfd = mem.handle();
        QVERIFY2(KSandbox::IO::isValidFd(memfd), QByteArray::number(memfd.fd).data());
        QImage image = KSandbox::imageFromSharedMemory(std::move(mem), 20, 20, QImage::Format_RGB32);
        QVERIFY(!image.isNull());
        QCOMPARE(image.byteCount(), 20 * 20 * 4);
        QVERIFY2(KSandbox::IO::isValidFd(memfd), QByteArray::number(memfd.fd).data());
    }
};

QTEST_MAIN(SharedMemoryTest)

#include "sharedmemorytest.moc"
