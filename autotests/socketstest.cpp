/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QTest>
#include <QDebug>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/socket.h>

#include "ksandbox/util.h"
#include <ksandbox/process.h>


class SocketsTest : public QObject {
    Q_OBJECT
private:
    void sendFds(const int count, int expected = -1) {
        if (expected == -1) {
            expected = count;
        }
        char value = count;
        KSANDBOX_SYSCALL(write(sizeSocket.fd, &value, 1));
        qDebug("Sender about to send %d fds", value);
        fflush(stderr);
        std::vector<int> fds;
        for (int i = 0; i < count; i++) {
            int next;
            KSANDBOX_SYSCALL(next = dup(nullFd));
            fds.push_back(next);
        }
        KSandbox::IO::sendFileDescriptors(fdSocket, fds);
        KSANDBOX_SYSCALL(read(sizeSocket.fd, &value, 1));
        for (int fd : fds) {
            close(fd);
        }
        QCOMPARE((int)value, expected);

    }
private Q_SLOTS:
    void testSending() {
        setpgid(0, 0); // this should kill receiver when we exit!
        // otherwise there is also a Linux specific prctl
        // TODO: create util function for this
        int sockets[2];
        int sizeSockets[2];
        KSANDBOX_SYSCALL(socketpair(AF_LOCAL, SOCK_STREAM, 0, sockets));
        KSANDBOX_SYSCALL(socketpair(AF_LOCAL, SOCK_STREAM, 0, sizeSockets));
        childPid = fork();
        if (childPid == -1) {
            QFAIL("fork failed");
        } else if (childPid == 0) {
            // child
            close(sockets[0]);
            close(sizeSockets[0]);
            fdSocket = KSandbox::SocketDescriptor(sockets[1]);
            sizeSocket = sizeSockets[1];
            KSandbox::setProcessName("socketstest-receiver");
            while (true) {
                char expected = 0;
                KSANDBOX_SYSCALL(read(sizeSocket.fd, &expected, 1));
                if (expected == -2) {
                    qDebug("about to exit receiver");
                    sleep(5);
                    qDebug("really exiting receiver");
                    exit(0);
                }
                qDebug("Receiver expecting %d fds", expected);
                fflush(stderr);
                auto fds = KSandbox::IO::receiveFileDescriptors(fdSocket);
                char result = fds.size();
                if (expected != result) {
                    qWarning("Expected %d fds, got %d instead", expected, result);
                } else {
                    qDebug("Received %d fds as expected", result);
                    qDebug() << QVector<int>::fromStdVector(fds);
                }
                for (int fd : fds) {
                    close(fd);
                }
                fflush(stderr);
                write(sizeSocket.fd, &result, 1);
            }

        } else {
            // parent
            KSandbox::setProcessName("socketstest sender");
            close(sockets[1]);
            close(sizeSockets[1]);
            fdSocket = KSandbox::SocketDescriptor(sockets[0]);
            sizeSocket = sizeSockets[0];
            nullFd = open("/dev/null", O_RDWR);

            sendFds(0);
            sendFds(1);
            sendFds(2);
            sendFds(4);
            sendFds(9);
            sendFds(10);
            sendFds(11, 10); // max of 10 by default
            sendFds(12, 10);
            char val = -2;
            sleep(5);
            write(sizeSocket.fd, &val, 1);
            sleep(1);
        }
    }

    void testSingleFd()
    {
        auto p = KSandbox::Process::fork([]() {
            // TODO: add an overload that accepts a void function and always returns 0
            []() {
                auto& proc = KSandbox::Process::self();
                int ready;
                read(proc.rpcSocket().fd, &ready, sizeof(ready));
                QCOMPARE(ready, 42);
                int fd = KSandbox::IO::receiveFileDescriptor(proc.fdTransferSocket()).fd;
                qDebug("Received FD");
                close(fd);
            }();
            return QTest::currentTestFailed() ? 1 : 0;
        });
        int ready = 42;
        KSandbox::IO::sendFileDescriptor(p.fdTransferSocket(), KSandbox::FileDescriptor{STDOUT_FILENO});
        write(p.rpcSocket().fd, &ready, sizeof(ready));
        int status;
        QVERIFY(p.wait(&status));
        QVERIFY(!p.isAlive());
        QVERIFY(WIFEXITED(status));
        QCOMPARE(WEXITSTATUS(status), 0);

    }

    void cleanupTestCase() {
        qDebug("Exiting main process");
    }
private:
    pid_t childPid;
    int nullFd;
    KSandbox::SocketDescriptor fdSocket;
    KSandbox::SocketDescriptor sizeSocket;
};

QTEST_APPLESS_MAIN(SocketsTest)

#include "socketstest.moc"
