#include <QCoreApplication>

#include <ksandbox/process.h>
#include <ksandbox/dbusutils.h>
#include "testifaceadaptor.h"

class DBusEchoVirtualObject : public QDBusVirtualObject {
public:
    virtual bool handleMessage(const QDBusMessage& message, const QDBusConnection& connection) override {
        qDebug() << "Received" << message << "from" << connection.name();
        auto reply = message.createReply(message.arguments());
        connection.send(reply);
        return true;
    }

    virtual QString introspect(const QString& path) const override {
        Q_UNUSED(path)
        return QString();
    }
private:
};

int main(int argc, char** argv) {
    // qputenv("LD_PRELOAD", "/home/alex/devel/dbus/cmake/build/lib/libdbus-1.so.3.16.4");
    // qputenv("DBUS_VERBOSE", "1");
    QCoreApplication app(argc, argv);
    KSandbox::DBus::Connection rpc = KSandbox::DBus::Connection::connectToParent();
    auto adaptor = new TestIfaceAdaptor(new TestIfaceImpl(&app));
    KSANDBOX_REQUIRE(rpc.exportObject("/dbustest", adaptor));
    qDebug() << "child client object:" << rpc.server().objectRegisteredAt("/dbustest");
    KSANDBOX_REQUIRE_NONNULL(rpc.server().objectRegisteredAt("/dbustest"));
    KSANDBOX_REQUIRE((rpc.client().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
    KSANDBOX_REQUIRE((rpc.server().connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
    KSANDBOX_REQUIRE(rpc.server().registerVirtualObject("/virtual", new DBusEchoVirtualObject));

//             auto msg = QDBusMessage::createMethodCall(QString(), QStringLiteral("/dummy"), QString(), QString("__noop"));
//             QDBusMessage response = dbusConnection.call(msg);
//             qDebug() << "parent reply to" << msg << "was" << response;
    // ! QCoreApplication already exists since we forked
    return app.exec();
}
