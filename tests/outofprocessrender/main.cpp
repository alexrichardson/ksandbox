/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QProcess>
#include <QDebug>
#include <QDirIterator>
#include <QStandardPaths>

#include <signal.h>
#include <sys/socket.h>

#include "common.h"

extern char **environ; // required on FreeBSD


//#define QT_QPA_PLATFORM "linuxfb" // works (but might have some issues if multiple processes use /dev/fb0)
//#define QT_QPA_PLATFORM "wayland" // works if weston is running
//#define QT_QPA_PLATFORM "minimalegl" // ERROR
//#define QT_QPA_PLATFORM "eglfs" // ERROR: (failed to open keyboard)
//#define QT_QPA_PLATFORM "minimal" // font rendering broken (appears to be unsupported by looking at the sources):
//#define QT_QPA_PLATFORM "offscreen" // font rendering broken: QFontDatabase: Cannot find font directory /usr/lib64/fonts - is Qt installed correctly?
                                      // need to set QT_QPA_FONTDIR, but even then it doesn't work
#define QT_QPA_PLATFORM "xcb" // works, but not really what we want


// XXX: this is all unsafe, if the child process goes crazy, but should be fine for a PoC

class Renderer {
public:
    Renderer() : shm(FILE_SIZE, KSandbox::SharedMemory::DefaultFlags, "pixmap_buf") {
        qDebug() << "shell SHM total size:" << shm.totalSize();
        int sockets[2];
        if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockets) == -1) {
            perror("socketpair");
            exit(1);
        }
        renderPid = fork();
        if (renderPid == -1) {
            perror("fork");
            exit(1);
        } else if (renderPid == 0) {
            // child
            close(sockets[0]);
            errno = 0;
            if (dup2(sockets[1], STDIN_FILENO) == -1) { perror("dup2 stdin"); }
            if (dup2(sockets[1], STDOUT_FILENO) == -1) { perror("dup2 stduot"); }
            if (dup2(shm.handle().fd, SHM_FD) == -1) { perror("dup2 shm"); }; // make sure the shm fd is 3

            // for some reason FD 10, 13 and 164 remain open, we need to close them
            // LINUX only: (similar to BSD closefrom())

            // let's try chroot
            int rendererExe = open("./renderer", O_RDONLY | O_CLOEXEC);
            Q_ASSERT(rendererExe != -1);
            QString dir = QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation) + QStringLiteral("/sandbox");
            Q_ASSERT(QDir().mkpath(dir));
            if (chdir(dir.toLocal8Bit().constData()) != 0) {
                perror("chdir");
            }

            const char* renderArgs[] = { "./renderer", "-platform", QT_QPA_PLATFORM, nullptr };

            setenv("QT_QPA_FONTDIR", "/usr/share", true);
            if (fexecve(rendererExe, const_cast<char**>(renderArgs), environ) == -1) {
                perror("execve");
                exit(3);
            }
        } else {
            // parent
            close(sockets[1]);
            renderProcSocket = sockets[0];
            // check that the process actually launched correctly (racy)
            sleep(1);
            int status;
            pid_t result = waitpid(renderPid, &status, WNOHANG);
            if (result != 0) {
                fprintf(stderr, "Renderer didn't launch properly: %d\n", WEXITSTATUS(status));
                exit(1);
            }
            qDebug() << "renderer started!";
            try {
                shm.map(FILE_SIZE, KSandbox::SharedMemory::Read);
            } catch(...) {
                kill(renderPid, SIGKILL);
                throw;
            }
            img.reset(new QImage(shm.begin(), IMAGE_WIDTH, IMAGE_HEIGHT, QImage::Format_RGB888));
            Q_ASSERT(img->byteCount() == FILE_SIZE);
        }
    }

    void render(QLabel* label) {
        if (write(renderProcSocket, "1", 1) != 1) {
            perror("Failed to write 1 byte to renderprocess");
            return;
        }
        qDebug() << "initiated render";
        char recvbuf[2];
        if (read(renderProcSocket, recvbuf, 1) != 1) {
            perror("Failed to read 1 byte from renderprocess");
            return;
        }
        qDebug() << "render completed";
        // TODO: remove copies and make async
        label->setPixmap(QPixmap::fromImage(*img));
        qDebug() << "set pixmap completed";
        label->update();
    }
    ~Renderer() {
        if (close(renderProcSocket) == 1) {
            perror("Failed to close renderprocess socket");
        }
        int status;
        pid_t result = waitpid(renderPid, &status, 0);
        if (result != renderPid) {
            perror("waitpid");
        }
        if (WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Renderer didn't exit properly: %d\n", WEXITSTATUS(status));
        }
    }

    pid_t pid() const {
        return renderPid;
    }
private:
    int renderProcSocket;
    pid_t renderPid;
    QScopedPointer<QImage> img;
    KSandbox::SharedMemory shm;
};

int main(int argc, char **argv) {
    QApplication app(argc, argv);
    QWidget window;
    QHBoxLayout* layout = new QHBoxLayout();
    QLabel* label = new QLabel(QStringLiteral("Noting to see here..."), &window);
    layout->addWidget(label);
    QPushButton* button = new QPushButton(QStringLiteral("Render"), &window);
    Renderer r;
    window.setWindowTitle(QStringLiteral("Main PID: ") + QString::number(getpid())
            + QStringLiteral(", renderer PID: ") + QString::number(r.pid()));
    QObject::connect(button, &QPushButton::clicked, button, [label, &r]() {
        qDebug("About to render");
        r.render(label);
    });
    layout->addWidget(button);
    window.setLayout(layout);
    window.show();
    return app.exec();
}
