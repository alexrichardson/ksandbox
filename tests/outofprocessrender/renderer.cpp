/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <stdio.h>
#include <random>
#include <iostream>
#include <QPainter>
#include <QImage>
#include <QFont>
#include <QDebug>
#include <QGuiApplication>

#include "common.h"


int main(int argc, char** argv) {
    QGuiApplication app(argc, argv); // required for text
    // FD 3 contains the shared memory file descriptor
    if (fcntl(SHM_FD, F_GETFD) == -1) {
        perror("SHM_FD is invalid");
        exit(1);
    }
    KSandbox::SharedMemory shm(KSandbox::FileDescriptor{SHM_FD});
    qDebug() << "renderer SHM total size:" << shm.totalSize();
    shm.map(FILE_SIZE, KSandbox::SharedMemory::Write);

    std::mt19937 mt;
    std::uniform_int_distribution<int> dist(0, 255);
    QImage image(shm.begin(), IMAGE_WIDTH, IMAGE_HEIGHT, QImage::Format_RGB888);
    QFont f;
    f.setBold(true);
    f.setPointSize(20);
    fprintf(stderr, "renderer waiting for request...\n");
    int frame = 0;
    while (getchar() != '\0') {
        frame++;
        fprintf(stderr, "renderer filling buffer... ");
        QPainter p(&image);
        QColor randomColour(dist(mt), dist(mt), dist(mt));
        QColor randomColour2(dist(mt), dist(mt), dist(mt));
        p.setPen(randomColour);
        p.fillRect(image.rect(), randomColour);
        QPen pen;
        pen.setWidth(5);
        pen.setColor(randomColour2);
        pen.setJoinStyle(Qt::MiterJoin);
        pen.setCapStyle(Qt::FlatCap);
        p.setPen(pen);
        p.drawRect(2, 2, IMAGE_WIDTH - 5, IMAGE_HEIGHT - 5);
//         p.setFont(f);
        p.setFont(f);
        pen.setColor(QColor(dist(mt), dist(mt), dist(mt)));
        p.setPen(pen);
//         p.setPen(Qt::green);
        p.drawText(15, 33, QStringLiteral("Renderer frame #") + QString::number(frame));
        p.end();
        //         for (size_t i = 0; i < FILE_SIZE; i++) {
        //             data[i] = dist(mt);
        //         }

        fprintf(stderr, "done.\n");
        shm.sync();
        fprintf(stderr, "msync() completed\n");
        write(STDOUT_FILENO, "x", 1); // signal that generating is done (and don't buffer!)
        fprintf(stderr, "wrote reply\n");
    }
    fprintf(stderr, "renderer exiting\n");
    return 0;
}
