/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>

#include "ksandbox/sharedmemory.h"

constexpr uint IMAGE_WIDTH = 400;
constexpr uint IMAGE_HEIGHT = 50;
constexpr size_t FILE_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT * 3;
constexpr int SHM_FD = 3;
