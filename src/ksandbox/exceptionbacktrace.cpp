/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

// override __cxa_throw to print a backtrace whenever an exception is thrown.
#include <cxxabi.h>
#include <execinfo.h>
#include <dlfcn.h>

#if HAVE_BACKTRACE
#include BACKTRACE_HEADER
#elif HAVE_LIBUNWIND
#include <libunwind.h>
#endif
#include <stdio.h>
#include <unistd.h>

#include <string>
#include <cstring>
#include <memory>


// http://stackoverflow.com/questions/11665829/how-can-i-print-stack-trace-for-caught-exceptions-in-c-code-injection-in-c
namespace {
//   void * last_frames[20] = {};
//   size_t last_size = 0;
  typedef std::unique_ptr<char,void(*)(void*)> FunctionName;
  // FunctionName exception_name{nullptr, &std::free};


  FunctionName demangle(const char *name) {
    int status;
    FunctionName ret{abi::__cxa_demangle(name, 0, 0, &status), &std::free};
    return status ? FunctionName{strdup(name), &std::free} : std::move(ret);
  }

void do_backtrace() {
#if HAVE_BACKTRACE
#define BACKTRACE_COUNT 40
    void* backtraceBuffer[BACKTRACE_COUNT];
    int count = backtrace(backtraceBuffer, BACKTRACE_COUNT);
    if (count > 0) {
        backtrace_symbols_fd(backtraceBuffer, count, STDERR_FILENO);
    }
#elif HAVE_LIBUNWIND
    unw_cursor_t    cursor;
    unw_context_t   context;

    unw_getcontext(&context);
    unw_init_local(&cursor, &context);

    while (unw_step(&cursor) > 0) {
        unw_word_t  offset, pc;
        char        fname[256];

        unw_get_reg(&cursor, UNW_REG_IP, &pc);

        fname[0] = '\0';
        (void) unw_get_proc_name(&cursor, fname, sizeof(fname), &offset);
        auto fnameDemangled = demangle(fname);

        if (!fnameDemangled || !fnameDemangled.get()[0]) {
            // for a newly spawned thread the lowest element is zero
            if (pc != 0) {
                fprintf(stderr, "    @%#lx\n", pc);
            }
        } else {
            fprintf(stderr, "    %s+%#lx\n", fnameDemangled.get(), offset);
        }
        // void* syms[1] = { (void*)pc };
        // backtrace_symbols_fd(syms, 1, STDERR_FILENO);
    }
#else
#error "No backtracer available, this file should not be built"
#endif
}

}


extern "C" {
  __attribute__((used)) void __cxa_throw(void *ex, void *_info, void (*dest)(void*)) {
    std::type_info* info = (std::type_info*)_info;
    FunctionName exception_name = demangle(info->name());
    // last_size = backtrace(last_frames, sizeof last_frames/sizeof(void*));
    static void (*const rethrow)(void*,void*,void(*)(void*)) = (void (*const)(void*,void*,void(*)(void*)))dlsym(RTLD_NEXT, "__cxa_throw");
    try {
        rethrow(ex, info, dest);
#if 0
    } catch (const kj::Exception& e) {
        std::string message = e.getFile();
        message += ":";
        message += std::to_string(e.getLine());
        message += ": ";
        message += e.getDescription().cStr();
        fprintf(stderr, "Exception in process %d of type %s: %s\n", getpid(), exception_name.get(), message.c_str());
        do_backtrace();
        throw;
#endif
    } catch (const std::exception& e){
        fprintf(stderr, "Exception in process %d of type %s: %s\n", getpid(), exception_name.get(), e.what());
        do_backtrace();
        throw;
    } catch (...) {
        fprintf(stderr, "Exception in process %d of type %s:\n", getpid(), exception_name.get());
        do_backtrace();
        throw;
    }
    __builtin_unreachable();
  }
}
