#pragma once

#cmakedefine01 HAVE_CAPSICUM_EMULATION
#cmakedefine01 HAVE_SYS_CAPSICUM_H
#cmakedefine01 HAVE_SYS_PROCDESC_H
#cmakedefine01 HAVE_BACKTRACE
#define BACKTRACE_HEADER <${Backtrace_HEADER}>
#cmakedefine01 HAVE_LIBUNWIND

/* for linux-bpf-capmode.c */
#cmakedefine HAVE_ASM_UNISTD_64_X32_H
#cmakedefine HAVE_ASM_UNISTD_64_AMD64_H
#cmakedefine HAVE_ASM_UNISTD_32_IA32_H

/* for libnv */
#cmakedefine HAVE_PJDLOG
#cmakedefine HAVE_STRUCT_UCRED
#cmakedefine HAVE_STRUCT_CMSGCRED

#define RPC_SOCKET_ENV_VAR "KSANDBOX_RPC_SOCKET"
/* TODO: remove once capnproto supports fd transfers */
#define FD_TRANSFER_SOCKET_ENV_VAR "KSANDBOX_FD_TRANSFER_SOCKET"
#define SANDBOXED_PROCESS_ENV_VAR "KSANDBOX_LAUNCHED_PROCESS"
#cmakedefine01 KSANDBOX_MAKE_CONNECTION_IN_NEW_THREAD
