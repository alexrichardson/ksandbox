/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2016  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "dbusutils.h"
#include "global.h"
#include "util.h"

// This is all based on DBus for now, but can hopefully be changed if we find a better solution

namespace KSandbox {

#define KSANDBOX_TOKENPASTE_IMPL(x, y) x ## y
#define KSANDBOX_TOKENPASTE(x, y) KSANDBOX_TOKENPASTE_IMPL(x, y)

// TODO: make the members private: this macro works as long as the class is not defined inside a namespace
// parentheses around the friend declaration are required to handle classes in namespaces
// weird C++ stuff http://stackoverflow.com/questions/2207219/how-do-i-define-friends-in-global-namespace-within-another-c-namespace
// #define KSANDBOX_SERIALIZED_MEMBERS(cls, args) \
// private: \
//     inline auto ksandbox_asConstTuple() const -> decltype(std::tie args) \
//     { return std::tie args; } \
//     inline auto ksandbox_asMutableTuple() -> decltype(std::tie args) \
//     { return std::tie args; } \
//     friend QDBusArgument& (::operator<<)(QDBusArgument& arg, const cls& c); \
//     friend const QDBusArgument& (::operator>>)(const QDBusArgument& arg, cls& c)

// would need to forward declare the operators as friend functions have to be in the same namespace or already declared
// just make these functions public for now (could also remove the class name from the macro...)
/** @warning Must be used inside the class declaration */
#define KSANDBOX_SERIALIZED_MEMBERS(cls, args) \
public: \
    inline auto ksandbox_asConstTuple() const -> decltype(std::tie args) \
    { return std::tie args; } \
    inline auto ksandbox_asMutableTuple() -> decltype(std::tie args) \
    { return std::tie args; } \
private:

/** @warning Must be used in the global namespace!*/
#define KSANDBOX_DECLARE_SERIALIZABLE_CLASS(cls) \
    Q_DECLARE_METATYPE(cls) \
    QDBusArgument& operator<<(QDBusArgument&, const cls&); \
    const QDBusArgument& operator>>(const QDBusArgument&, cls&)

#ifndef KSANDBOX_VERBOSE_DEBUG
#define KSANDBOX_REGISTER_DBUS_TYPE(type) qDBusRegisterMetaType<type>()
#else
#define KSANDBOX_REGISTER_DBUS_TYPE(type) []() { \
        int result = qDBusRegisterMetaType<type>(); \
        qDebug("Registered type " #type " for DBus with signature '%s'", QDBusMetaType::typeToSignature(qMetaTypeId<type>())); \
        return result; \
    }()
#endif

/** @warning Must be used in the global namespace!*/
#define KSANDBOX_SERIALIZABLE_CLASS_DEFINITION(cls) \
    static const int KSANDBOX_TOKENPASTE(DBusMetaType, __COUNTER__) = KSANDBOX_REGISTER_DBUS_TYPE(cls); \
    QDBusArgument& operator<<(QDBusArgument& arg, const cls& c) \
    { return ::KSandbox::DBus::impl::serialize(arg, c.ksandbox_asConstTuple()); } \
    const QDBusArgument& operator>>(const QDBusArgument& arg, cls& c) \
    { return ::KSandbox::DBus::impl::deserialize(arg, c.ksandbox_asMutableTuple()); }

template<typename T>
QVariant serialize(const T& value) {
    static_assert(sizeof(&T::ksandbox_asConstTuple), "Class is missing KSANDBOX_SERIALIZED_MEMBERS declaration");
    QDBusArgument arg;
    ::KSandbox::DBus::impl::serialize(arg, value.ksandbox_asConstTuple());
    return QVariant::fromValue(arg);
    QT_STRINGIFY(sdas);
}
template<typename T>
QVariant serialize(const T* value) {
    static_assert(sizeof(&T::ksandbox_asConstTuple), "Class is missing KSANDBOX_SERIALIZED_MEMBERS declaration");
    QDBusArgument arg;
    ::KSandbox::DBus::impl::serialize(arg, value->ksandbox_asConstTuple());
    return QVariant::fromValue(arg);
}

template<typename T>
T deserialize(const QVariant& data) {
    if (data.canConvert<QDBusArgument>()) {
        T result;
        static_assert(sizeof(&T::ksandbox_asMutableTuple), "Class is missing KSANDBOX_SERIALIZED_MEMBERS declaration");
        ::KSandbox::DBus::impl::deserialize(data.value<QDBusArgument>(), result.ksandbox_asConstTuple());
        return result;
    } else {
        KSANDBOX_REQUIRE(data.canConvert<T>(), data.type(), qMetaTypeId<T>());
        return data.value<T>();
    }
}

template<typename T>
T* deserialize(const QVariant& data) {
    if (data.canConvert<QDBusArgument>()) {
        T* result = new T();
        static_assert(sizeof(&T::ksandbox_asMutableTuple), "Class is missing KSANDBOX_SERIALIZED_MEMBERS declaration");
        ::KSandbox::DBus::impl::deserialize(data.value<QDBusArgument>(), result->ksandbox_asConstTuple());
        return result;
    } else {
        KSANDBOX_REQUIRE(data.canConvert<T*>(), data.type(), qMetaTypeId<T*>());
        return data.value<T*>();
    }
}

}
