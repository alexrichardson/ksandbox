/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"
#include "dbusutils.h"
#include "process.h"
#include "debug_p.h"

#include <QCoreApplication>
#include <QDBusServer>
#include <QDBusArgument>
#include <QDBusError>
#include <QFileInfo>
#include <QElapsedTimer>


// FIXME: remove all this private API stuff
#ifdef QT_NO_SIGNALS_SLOTS_KEYWORDS
#define slots Q_SLOTS
#define signals Q_SIGNALS
#endif
#include <QtDBus/private/qdbusconnection_p.h>

#include <unistd.h>

KSandbox::DBus::Connection::~Connection()
{
}

bool KSandbox::DBus::Connection::exportObject(const QString& path, QObject* object)
{
    return m_server.registerObject(path, object, QDBusConnection::ExportAllContents);
}

static void workAroundDBusP2PBug(QDBusConnection& conn)
{
    qCDebug(KSANDBOX) << "working around QtDbus::connectToPeer() bug";
    QDBusConnectionPrivate* priv = QDBusConnectionPrivate::d(conn);
    priv->capabilities |= QDBusConnection::UnixFileDescriptorPassing;
    KSANDBOX_REQUIRE((conn.connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing));
}

KSandbox::DBus::Connection KSandbox::DBus::Connection::connectToChild(const KSandbox::Process& child)
{
    Connection result;

    // TODO: find a way to make dbus use the socketpair..

    // Annoying API limitation: we need to keep the QDBusServer alive even after we have established
    // the connections as ~QDBusServer will destroy all connections made through it...
#if 0
    SocketDescriptor dbusSocket = child.rpcSocket();
    auto potentialDbusPath = [](const QString& path) -> QString {
        if (QFile::exists(path)) {
            return QStringLiteral("unix:path=") + path;
        }
        return QString();
    };
    QString address = potentialDbusPath(QStringLiteral("/proc/self/fd/") + QString::number(dbusSocket.fd));
    if (address.isEmpty()) {
        address = potentialDbusPath(QStringLiteral("/dev/fd/") + QString::number(dbusSocket.fd));
    }
    qCDebug(KSANDBOX) << "creating new dbus server listening on" << address;
    result.m_dbusServer.reset(new QDBusServer(address));
#else
    result.m_dbusServer.reset(new QDBusServer());
#endif
    result.m_dbusServer->setAnonymousAuthenticationAllowed(true);

    QString addr = result.m_dbusServer->address();
    qCDebug(KSANDBOX) << "parent to" << child.pid() << "server last err " << result.m_dbusServer->lastError() << ", address =" << addr;
    KSANDBOX_REQUIRE(result.m_dbusServer->isConnected());

    uint connections = 0;
    auto slotConnection = QObject::connect(result.m_dbusServer.get(), &QDBusServer::newConnection,
                                           result.m_dbusServer.get(), [&](const QDBusConnection& conn) {
        qCDebug(KSANDBOX) << "connection from child made:" << conn.name() << conn.connectionCapabilities();
        if (connections == 0) {
            result.m_client = conn;
            workAroundDBusP2PBug(result.m_client);
        } else if (connections == 1) {
            result.m_server = conn;
            workAroundDBusP2PBug(result.m_server);
        } else {
            qCCritical(KSANDBOX) << "Sandboxed child" << child.pid() << "attempted to create more than 2 DBUS connections."
                    "This should never happen! Child could be malicious!";
        }
        qCDebug(KSANDBOX) << "connection from child made after workaround:" << conn.name() << conn.connectionCapabilities();
        connections++;
    });
    // TODO: send nonce to make sure only the actual parent authenticates
    uint32_t nonce = KSandbox::secureRandomNumber();
    // addr += "\n\n";
    // addr += QString::number(nonce, 16);
    // tell parent process that we're ready
    KSANDBOX_SYSCALL(::write(child.fdTransferSocket().fd, addr.constData(), addr.size() * 2));
    // wait for connection
    QElapsedTimer e;
    // TODO: is there a better solution than this loop?
    e.start();
    while (connections < 2 && e.elapsed() < 10000) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 1000);
    }
    KSANDBOX_REQUIRE_EQUAL(connections, 2, " Child did not make 2 connections in ", e.elapsed(), "ms");
    // we no longer need to accept connections now
    KSANDBOX_REQUIRE(result.m_dbusServer->disconnect(slotConnection));
    result.m_dbusServer->setAnonymousAuthenticationAllowed(false); // hopefully this stops further connections
    // TODO: ideally we could just drop the listening socket now, or even better reuse the socketpair that we already have...

    qCDebug(KSANDBOX) << "parent to" << child.pid() << "client last err "
            << result.m_client.lastError() << "client connection name:" << result.m_client.name();
    qCDebug(KSANDBOX) << "parent to" << child.pid() << "server last err " << result.m_server.lastError()
            << "client connection name:" << result.m_server.name();
    KSANDBOX_REQUIRE(result.m_client.isConnected(), DBG_VAR(result.m_client.lastError()));
    KSANDBOX_REQUIRE(result.m_server.isConnected(), DBG_VAR(result.m_server.lastError()));
    // TODO: export a default interface
    return result;
}

static QDBusConnection connectToPeerImpl(QString address, QString name)
{
    qDebug() << "connectToPeer calling";
    QDBusConnection result = QDBusConnection::connectToPeer(address, name);
    qDebug() << "connectToPeer done";
    qCDebug(KSANDBOX) << "dbus: " << name << " last err " << result.lastError()
        << "conn name:" << result.name();
    KSANDBOX_REQUIRE(result.isConnected(), DBG_VAR(result.lastError()));
    if (address.startsWith(QStringLiteral("unix:"))
            && !(result.connectionCapabilities() & QDBusConnection::UnixFileDescriptorPassing)) {
        workAroundDBusP2PBug(result);
    }
    return result;
}

#if KSANDBOX_MAKE_CONNECTION_IN_NEW_THREAD != 0
#include <future>
static std::future<QDBusConnection> connectToPeer(QString address, QString name)
{
    return std::async(std::launch::async, connectToPeerImpl, address, name);
}
#else
auto connectToPeer = connectToPeerImpl;
#endif


KSandbox::DBus::Connection KSandbox::DBus::Connection::connectToParent()
{

    QChar buffer[1024];
    int size = -1;
    KSANDBOX_SYSCALL(size = read(KSandbox::Process::self().fdTransferSocket().fd, buffer, sizeof(buffer)));
    QString remoteAddr(buffer, size / 2);
    qCDebug(KSANDBOX) << "parent remote addr =" << remoteAddr << "size =" << size << "strlen =" << remoteAddr.length();
    qDebug() << "PRocessing events:";
    // TODO: nonce
    QCoreApplication::processEvents(QEventLoop::AllEvents, 1000);
    qDebug() << "PRocessed events:";

    // we need two connections to prevent deadlocking
    Connection result;
    auto server = connectToPeer(remoteAddr, QStringLiteral("sandboxed-server"));
    auto client = connectToPeer(remoteAddr, QStringLiteral("sandboxed-client"));
#if KSANDBOX_MAKE_CONNECTION_IN_NEW_THREAD
    result.m_server = server.get();
    result.m_client = client.get();
#else
    result.m_server = server;
    result.m_client = client;
#endif
    return result;
}


static QVariantMap removeQDBusArguments(const QVariantMap& map) {
    QVariantMap result;
    for (auto it = map.begin(), end = map.end(); it != end; ++it) {
        result.insert(it.key(), KSandbox::DBus::removeQDBusArgument(it.value()));
    }
    return result;
}

static QVariantList removeQDBusArguments(const QVariantList& list) {
    QVariantList result;
    result.reserve(list.size());
    for (const QVariant& v : list) {
        result << KSandbox::DBus::removeQDBusArgument(v);
    }
    return result;
}

QVariant KSandbox::DBus::removeQDBusArgument(const QVariant& in) {
    if (in.canConvert<QDBusArgument>()) {
        const QDBusArgument d = in.value<QDBusArgument>();
        switch (d.currentType()) {
            case QDBusArgument::ArrayType:
            {
                // qDebug() << "array:" << d.currentSignature();
                QVariantList list;
                d.beginArray();
                while (!d.atEnd()) {
                    // qDebug() << "a type:" << d.currentType() << d.currentSignature();
                    QVariant variant = d.asVariant();
                    if (!variant.isValid()) {
                        qCWarning(KSANDBOX) << "decoded invalid dbus variant!";
                        break;
                    }
                    QVariant decoded = removeQDBusArgument(variant);
                    // qDebug() << "a value" << variant << "aka" << decoded;
                    list << decoded;
                }
                d.endArray();
                //qDebug() << "a tmp" << list << d.currentSignature() << d.currentType();
                return list;
            }
            case QDBusArgument::MapType:
            {
                //qDebug() << "map:" << d.currentSignature();
                QVariantMap map;
                d.beginMap();
                while (!d.atEnd()) {
                    d.beginMapEntry();
                    //qDebug() << "m key type:" << d.currentType() << d.currentSignature();
                    QVariant key = d.asVariant();
                    //qDebug() << "m value type:" << d.currentType() << d.currentSignature();
                    QVariant value = d.asVariant();
                    QVariant valueDecoded = removeQDBusArgument(value);
                    //qDebug() << "m decoded" << key << "=" << value << "aka" << valueDecoded;
                    if (key.type() != QVariant::String) {
                        qCWarning(KSANDBOX) << "FIXME: non-string map entry not yet supported!";
                    }
                    d.endMapEntry();
                    if (!key.isValid() || !value.isValid()) {
                        qCWarning(KSANDBOX) << "decoded invalid dbus variant!";
                        break;
                    }
                    map.insert(key.toString(), valueDecoded);
                }
                d.endMap();
                // qDebug() << "m tmp" << map << d.currentSignature() << d.currentType();
                return map;
            }
            case QDBusArgument::BasicType:
            case QDBusArgument::VariantType:
                return removeQDBusArgument(d.asVariant());
            case QDBusArgument::StructureType:
            {
                //qDebug() << "structure:" << d.currentType() << d.currentSignature();
                QVariantList list;
                d.beginStructure();
                while (!d.atEnd()) {
                    //qDebug() << "s type:" << d.currentType() << d.currentSignature();
                    QVariant variant = d.asVariant();
                    if (!variant.isValid()) {
                        qCWarning(KSANDBOX) << "decoded invalid dbus variant!";
                        break;
                    }
                    QVariant decoded = removeQDBusArgument(variant);
                    //qDebug() << "s value" << variant << "aka" << decoded;
                    list << decoded;
                }
                d.endStructure();
                //qDebug() << "s tmp" << list << d.currentSignature() << d.currentType();
                return list;
            }
            default:
                qCWarning(KSANDBOX) << "Unhandled dbus type"
                    << d.currentType() << d.currentSignature();
                return QVariant();
        }
    } else if (in.canConvert<QDBusVariant>()) {
        return removeQDBusArgument(in.value<QDBusVariant>().variant());
    } else if (in.canConvert<QVariantMap>()) {
        return removeQDBusArguments(in.toMap());
    } else if (in.canConvert<QVariantList>()) {
        return removeQDBusArguments(in.toList());
    } else {
        return in;
    }
}

QVariant KSandbox::DBus::toQVariant(const QDBusMessage& reply)
{
    const auto args = reply.arguments();
    if (args.isEmpty()) {
        return QVariant();
    }
    KSANDBOX_REQUIRE_EQUAL(args.size(), 1);
    return removeQDBusArgument(args[0]);
}

QVariantList KSandbox::DBus::toQVariantList(const QDBusMessage& reply)
{
    return removeQDBusArguments(reply.arguments());
}


