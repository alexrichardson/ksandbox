/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <QDBusConnection>
#include <QDBusServer>
#include <QDBusArgument>
#include <QDBusMetaType>

#include <memory>
#include <vector>
#include <array>
#include <list>
#include <deque>

#include "ksandbox_export.h"
#include "util.h"

namespace KSandbox {

class Process;

/** @warning All these functions are NOT THREAD SAFE (yet)!! */
namespace DBus {
/**
 * For now we have two separate connections as sending and receiving requests on a
 * single connection appears to deadlock.
 *
 * All requests should be made on the client connection and any interfaces that are exported
 * should use the server connection.
 *
 * @warning DO NOT store this as a global static object, it seems like the
 * QDBusConnection mustn't outlive QCoreApplication::instance()
 */
class KSANDBOX_EXPORT Connection {
    Q_DISABLE_COPY(Connection)
public:
    Connection() : m_client(QString()), m_server(QString()) {};
    ~Connection();
    Connection(Connection&&) = default;
    Connection& operator=(Connection&& c) {
        std::swap(m_client, c.m_client);
        std::swap(m_server, c.m_server);
        std::swap(m_dbusServer, c.m_dbusServer);
        return *this;
    }
    /**
     * Just to make sure the right QDBusConnection is used
     * @see QDBusConnection::registerObject()
     */
    bool exportObject(const QString& path, QObject* object);

    // TODO: add this?
    // QDBusConnection& operator->(){ return m_client; }

    // should not be needed often and since they are explicitly shared a copy is fine
    inline QDBusConnection& server() { return m_server; }
    inline QDBusConnection& client() { return m_client; }


    /** Connects to the child process @p child
     * @warning must be called before the child enters the sandbox. The child must also call
     * connectToParent() otherwise this will block possibly forever
     *
     * TODO: do this automatically unless user opts out?
     */
    static Connection connectToChild(const KSandbox::Process& child);
    static Connection connectToParent();
private:
    // this needs to be kept alive because the QDBusServer destructor removes all connections
    // that were made through it in the destructor
    // TODO: use shared_ptr to allow copying?
    std::unique_ptr<QDBusServer> m_dbusServer;
    QDBusConnection m_client;
    QDBusConnection m_server;
};

/**
 * Recursively converts the arguments of @p reply to a QVariantList that does not contain
 * the QDBusArgument type anymore.
 * This is useful if the method returns something like a QVariantMap that is potentially nested
 * (the "a{sv}" DBus type)
 */
KSANDBOX_EXPORT QVariantList toQVariantList(const QDBusMessage& reply);

/** Almost the same as toQVariantList() but requires that @p reply only has one argument
 * @see toQVariantList()
 */
KSANDBOX_EXPORT QVariant toQVariant(const QDBusMessage& reply);

KSANDBOX_EXPORT QVariant removeQDBusArgument(const QVariant& in);

// prevent errors in dbusutils.cpp (could cause infinite recursion)
QVariant removeQDBusArgument(const QVariantList& in) = delete;
QVariant removeQDBusArgument(const QVariantMap& in) = delete;


namespace impl {
// based on http://stackoverflow.com/questions/5768511/using-sfinae-to-check-for-global-operator
namespace has_dbus_insertion_operator_impl {
  typedef char no;
  typedef char yes[2];

  struct any_t {
    template<typename T> any_t( T const& );
  };

  no operator<<( QDBusArgument&, any_t const& );

  yes& test( QDBusArgument& );
  no test( no );

  template<typename T>
  struct has_insertion_operator {
    static QDBusArgument &s;
    static T const &t;
    static bool const value = sizeof( test(s << t) ) == sizeof( yes );
  };
}

template<typename T>
struct has_dbus_insertion_operator :
  has_dbus_insertion_operator_impl::has_insertion_operator<T> {
};


// to make sure the type name is included
// for now assume operator<< implies that operator>> also exists
// if not it will only be a less readable error message
template<typename T>
void verifyHasDbusStreamOperator() {
    static_assert(has_dbus_insertion_operator<T>::value, "Missing operator<<(QDBusArgument&, T&)");
}

// http://stackoverflow.com/questions/1198260/iterate-over-tuple
// TODO: is evaluation order guaranteed here? or do I need that int[] technique?
template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
writeToDbusArgument(QDBusArgument& arg, const std::tuple<Tp...>& t) {
    Q_UNUSED(arg)
    Q_UNUSED(t)
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
writeToDbusArgument(QDBusArgument& arg, const std::tuple<Tp...>& t)
{
    verifyHasDbusStreamOperator<decltype(std::get<I>(t))>();
    arg << std::get<I>(t);
    writeToDbusArgument<I + 1, Tp...>(arg, t);
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
readFromDbusArgument(const QDBusArgument& arg, const std::tuple<Tp...>& t) {
    Q_UNUSED(arg)
    Q_UNUSED(t)
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
readFromDbusArgument(const QDBusArgument& arg, const std::tuple<Tp...>& t)
{
    // FIXME: actually check operator>> instead of operator<<
    // for now we just assume symmetry
    verifyHasDbusStreamOperator<decltype(std::get<I>(t))>();
    arg >> std::get<I>(t);
    readFromDbusArgument<I + 1, Tp...>(arg, t);
}

template<typename... Tp>
QDBusArgument& serialize(QDBusArgument& arg, const std::tuple<Tp...>& t) {
    arg.beginStructure();
    writeToDbusArgument(arg, t);
    arg.endStructure();
    return arg;
}

template<typename... Tp>
inline const QDBusArgument& deserialize(const QDBusArgument& arg, const std::tuple<Tp...>& t) {
    arg.beginStructure();
    readFromDbusArgument(arg, t);
    arg.endStructure();
    return arg;
}

template<template <typename, typename> class STLContainer, typename T, typename Alloc>
inline QDBusArgument& serializeSTLContainer(QDBusArgument& arg, const STLContainer<T, Alloc>& list)
{
    int id = qMetaTypeId<T>();
    arg.beginArray(id);
    auto it = list.begin();
    auto end = list.end();
    for ( ; it != end; ++it)
        arg << *it;
    arg.endArray();
    return arg;
}

template<template <typename, typename> class STLContainer, typename T, typename Alloc>
inline const QDBusArgument& deserializeSTLContainer(const QDBusArgument& arg, STLContainer<T, Alloc>& list)
{
    arg.beginArray();
    list.clear();
    while (!arg.atEnd()) {
        T item;
        arg >> item;
        list.push_back(item);
    }

    arg.endArray();
    return arg;
}

} // namespace impl

} // namespace DBus
} // namespace KSandbox


// STL containers have two template parameters (type and allocator)
// however this is too generic as it might match a map class and then we serialize wrongly
// std::vector
template<typename T, typename Alloc> inline QDBusArgument& operator<<(QDBusArgument& arg, const std::vector<T, Alloc>& vec)
{ return KSandbox::DBus::impl::serializeSTLContainer(arg, vec); }
template<typename T, typename Alloc> inline const QDBusArgument &operator>>(const QDBusArgument &arg, std::vector<T, Alloc>& vec)
{ return KSandbox::DBus::impl::deserializeSTLContainer(arg, vec);}
// FIXME: why doesn't this work
// static_assert(KSandbox::DBus::impl::has_dbus_insertion_operator<std::vector<int>>::value, "DBus operator overload for std::vector<int> should exist now");
// std::list
template<typename T, typename Alloc> inline QDBusArgument& operator<<(QDBusArgument& arg, const std::list<T, Alloc>& vec)
{ return KSandbox::DBus::impl::serializeSTLContainer(arg, vec); }
template<typename T, typename Alloc> inline const QDBusArgument &operator>>(const QDBusArgument &arg, std::list<T, Alloc>& vec)
{ return KSandbox::DBus::impl::deserializeSTLContainer(arg, vec); }
// std::deque
template<typename T, typename Alloc> inline QDBusArgument& operator<<(QDBusArgument& arg, const std::deque<T, Alloc>& vec)
{ return KSandbox::DBus::impl::serializeSTLContainer(arg, vec); }
template<typename T, typename Alloc> inline const QDBusArgument &operator>>(const QDBusArgument &arg, std::deque<T, Alloc>& vec)
{ return KSandbox::DBus::impl::deserializeSTLContainer(arg, vec); }
// std::array
template<typename T, size_t Size> inline QDBusArgument& operator<<(QDBusArgument& arg, const std::array<T, Size>& array)
{
    int id = qMetaTypeId<T>();
    arg.beginArray(id);
    auto it = array.begin();
    auto end = array.end();
    for ( ; it != end; ++it)
        arg << *it;
    arg.endArray();
    return arg;
}
template<typename T, size_t Size> inline const QDBusArgument &operator>>(const QDBusArgument &arg, std::array<T, Size>& array)
{
    arg.beginArray();
    // array does not have a clear member
    for (size_t i = 0; i < Size; ++i) {
        if (!arg.atEnd()) {
            T item;
            arg >> item;
            array[i] = std::move(item);
        } else {
            array[i] = T();
        }
    }
    arg.endArray();
    return arg;
}

// TODO: std::set, QSet, std::map, std::unordered_map, std::unordered_set, std::forward_list

template<typename T, int Size>
inline QDBusArgument &operator<<(QDBusArgument &arg, const QVarLengthArray<T, Size> &list)
{
    int id = qMetaTypeId<T>();
    arg.beginArray(id);
    auto it = list.begin();
    auto end = list.end();
    for ( ; it != end; ++it)
        arg << *it;
    arg.endArray();
    return arg;
}

template<typename T, int Size>
inline const QDBusArgument &operator>>(const QDBusArgument &arg, QVarLengthArray<T, Size> &list)
{
    arg.beginArray();
    list.clear();
    while (!arg.atEnd()) {
        T item;
        arg >> item;
        list.push_back(item);
    }
    arg.endArray();
    return arg;
}
