/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QImage>

#include <memory>

#include "sharedmemory.h"

namespace KSandbox {
    /** Takes ownership of the Shared memory file descriptor */
    QImage imageFromSharedMemory(KSandbox::SharedMemory&& mem, size_t width, size_t height, QImage::Format format);
    QImage imageFromSharedMemory(std::unique_ptr<KSandbox::SharedMemory> mem, size_t width, size_t height, QImage::Format format);
}
