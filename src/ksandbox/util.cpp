/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "util.h"
#include "config.h"
#include "debug_p.h"
#include <QString>
#include <QVarLengthArray>
#include <QDirIterator>

#include <stdexcept>

#include <libgen.h> // dirname()
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <limits.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>

#if HAVE_SYS_CAPSICUM_H
#include <sys/capsicum.h>
#elif defined(__FreeBSD__) && __FreeBSD__ >= 9
// TODO: require recent enough FreeBSD 10 which has sys/capsicum
// use the old header that conflicts with a Linux header name
#include <sys/capability.h>
#elif HAVE_CAPSICUM_EMULATION
extern "C" int cap_enter(void);
extern "C" int cap_getmode(unsigned int *mode);
extern "C" bool cap_sandboxed(void);
#else
#define CAPSICUM_MISSING 1
#endif

#ifdef __linux__
#include <sys/prctl.h>
#include <linux/random.h>
#include <sys/syscall.h>
#elif defined(__FreeBSD__)
#include <stdlib.h> // arc4random()
#endif


Q_LOGGING_CATEGORY(KSANDBOX, "ksandbox", QtDebugMsg)


QByteArray KSandbox::secureRandomBuffer(size_t size)
{
    KSANDBOX_REQUIRE(size <= std::numeric_limits<int>::max());
    QByteArray buffer{int(size), Qt::Uninitialized};
#ifdef __linux__
    // no libc wrapper exists yet
    KSANDBOX_SYSCALL(syscall(__NR_getrandom, buffer.data(), buffer.size(), 0));
#else
    // TODO: use something that works in a sandbox
    result = arc4random_buf(buffer.data(), buffer.size());
#endif
    return buffer;
}

uint32_t KSandbox::secureRandomNumber()
{
    uint32_t result;
#ifdef __linux__
    // no libc wrapper exists yet
    KSANDBOX_SYSCALL(syscall(__NR_getrandom, &result, sizeof(result), 0));
#else
    // TODO: use something that works in a sandbox
    result = arc4random();
#endif
    return result;
}



KSandbox::Exception::Exception(QLatin1String str)
    : msg(str), dbg(&msg)
{

}

KSandbox::Exception::Exception(const QString& str)
    : msg(str), dbg(&msg)
{
}

KSandbox::Exception::~Exception()
{
}



KSandbox::SyscallException::SyscallException(QLatin1String callStr, int error)
    : Exception(callStr), code(std::error_code(error, std::system_category()))
{
    msg += QStringLiteral(" failed: ");
    auto errnoMsg = code.message();
    msg += QLatin1String(errnoMsg.data(), errnoMsg.size());
    msg += QLatin1String(".");
}

KSandbox::SyscallException::~SyscallException()
{
}
KSandbox::RequirementFailed::~RequirementFailed()
{
}



void KSandbox::IO::setNonblocking(int fd, bool nonblocking) {
    int flags;
    KSANDBOX_SYSCALL(flags = fcntl(fd, F_GETFL));
    if (nonblocking) {
        if ((flags & O_NONBLOCK) == 0) {
            KSANDBOX_SYSCALL(fcntl(fd, F_SETFL, flags | O_NONBLOCK));
        }
    } else {
        // remove the O_NONBLOCK flags
        if ((flags & O_NONBLOCK) != 0) {
            KSANDBOX_SYSCALL(fcntl(fd, F_SETFL, flags & ~O_NONBLOCK));
        }
    }
}

void KSandbox::IO::setCloseOnExec(int fd) {
  int flags;
  KSANDBOX_SYSCALL(flags = fcntl(fd, F_GETFD));
  if ((flags & FD_CLOEXEC) == 0) {
    KSANDBOX_SYSCALL(fcntl(fd, F_SETFD, flags | FD_CLOEXEC));
  }
}

bool KSandbox::IO::isSocketFd(int fd) {
    struct stat statbuf;
    int err = fstat(fd, &statbuf);
    return err == 0 && S_ISSOCK(statbuf.st_mode);
}

bool KSandbox::IO::isValidFd(FileDescriptor f)
{
    if (fcntl(f.fd, F_GETFL) >= 0) {
        return true;
    }
    // !! EBADF and not EBADFD ...
    return errno != EBADF;
}


void KSandbox::IO::closefrom(int fromFd) {
#ifdef Q_OS_FREEBSD
    ::closefrom(fromFd);
#else
    // TODO: use readdir / getdents since it is faster?
    QDirIterator fdIter(QStringLiteral("/proc/self/fd"));
    while (fdIter.hasNext()) {
        fdIter.next();
        bool okay;
        int fd = fdIter.fileName().toInt(&okay, 10);
        if (okay && fd > fromFd) {
            if (close(fd) == -1) {
                qWarning("close failed on %d: %s", fd, strerror(errno));
            }
        }
    }
#endif
}

QByteArray KSandbox::IO::executableDir() {
    char buf[PATH_MAX + 1];
    buf[0] = '\0';
    ssize_t length;
#ifdef Q_OS_FREEBSD
    KSANDBOX_SYSCALL(length = readlink("/compat/linux/proc/self/exe", buf, sizeof(buf) - 1));
#else
    KSANDBOX_SYSCALL(length = readlink("/proc/self/exe", buf, sizeof(buf) - 1));
#endif
    return QByteArray(dirname(buf));
}

// TODO: use forkfd from Qt

KSandbox::ExitStatus KSandbox::waitForExit(pid_t pid) {
    int status;
    pid_t r = KSANDBOX_SYSCALL_NOEXCEPT(waitpid(pid, &status, 0));
    if (r != pid) {
        KSANDBOX_FAILED_SYSCALL("waitpid", errno);
    }
    ExitStatus result;
    if (WIFEXITED(status)) {
        result.exitCode = WEXITSTATUS(status);
        result.exitedNormally = true;
    }
    if (WIFSIGNALED(status)) {
        result.signal = WTERMSIG(status);
        result.signalled = true;
    }
    result.pid = pid;
    return result;
}

void KSandbox::enterCapabilityMode() {
#ifdef CAPSICUM_MISSING
#warning "cap_enter() not available!"
    qCWarning(KSANDBOX, "cap_enter() not available!");
#else
    int err;
    // TODO: throw exception if it fails
    KSANDBOX_SYSCALL_NOEXCEPT(err = cap_enter());
    if (err == 0) {
        qCDebug(KSANDBOX, "Capability mode entered!");
    }
#endif
}

// based on:
// http://keithp.com/blogs/fd-passing/
// http://www.normalesup.org/~george/comp/libancillary/
ssize_t KSandbox::IO::sendFileDescriptors(SocketDescriptor socket, const std::vector<int>& fds)
{
    struct msghdr msg;
    char dummyData[4] = { 'f', 'd', 's', '\0' };
    struct iovec dummyIOV;
    dummyIOV.iov_base = dummyData;
    dummyIOV.iov_len = sizeof(dummyData);

    msg.msg_name = nullptr;
    msg.msg_namelen = 0;
    msg.msg_iov = &dummyIOV;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;
    // TODO: get rid of VLA? QVarLengthArray?

    // QVarLengthArray should have the right alignment
    Q_STATIC_ASSERT(alignof(QVarLengthArray<char, 256>) > 4);
    QVarLengthArray<char, 256> controlBuffer(CMSG_SPACE(sizeof(int) * fds.size()));
    msg.msg_control = controlBuffer.data();
    msg.msg_controllen = controlBuffer.size();

    struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    uint sent = 0;
    for (int fd : fds) {
        if (fd != -1) {
            // qCDebug(KSANDBOX, "passing fd %d", fd);
            ((int *)CMSG_DATA(cmsg))[sent] = fd;
            sent++;
        } else {
            qCWarning(KSANDBOX, "not passing fd since it was invalid");
        }
    }
    cmsg->cmsg_len = CMSG_LEN(sizeof(int) * sent); // WTF why doesn't CMSG_LEN work here
    msg.msg_controllen = cmsg->cmsg_len;
    if (sent == 0) {
        qCWarning(KSANDBOX, "No valid file descriptors passed to KSandbox::IO::sendFileDescriptors()");
        msg.msg_control = nullptr;
        msg.msg_controllen = 0;
    }

    ssize_t ret;
    KSANDBOX_SYSCALL(ret = sendmsg(socket.fd, &msg, 0));
    return ret;
}

std::vector<int> KSandbox::IO::receiveFileDescriptors(SocketDescriptor socket, uint maxFds)
{
    std::vector<int> result;
    struct msghdr msg;
    char dummyData[4] = { '\0', '\0', '\0', '\0' };
    struct iovec dummyIOV;
    dummyIOV.iov_base = dummyData;
    dummyIOV.iov_len = sizeof(dummyData);

    msg.msg_name = nullptr;
    msg.msg_namelen = 0;
    msg.msg_iov = &dummyIOV;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;

    Q_STATIC_ASSERT(alignof(QVarLengthArray<char, 256>) > 4);
    QVarLengthArray<char, 256> controlBuffer(CMSG_SPACE(sizeof(int) * maxFds));
    msg.msg_control = controlBuffer.data();
    msg.msg_controllen = controlBuffer.size();

    struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
//     cmsg->cmsg_len = msg.msg_controllen;
//     cmsg->cmsg_level = SOL_SOCKET;
//     cmsg->cmsg_type = SCM_RIGHTS;
    // make sure the fd array is invalid before receiving
    for(uint i = 0; i < maxFds; i++) {
        ((int *)CMSG_DATA(cmsg))[i] = -1;
    }

    ssize_t size = 0;
    KSANDBOX_SYSCALL(size = recvmsg (socket.fd, &msg, 0));
    if (size != 4) {
        qCWarning(KSANDBOX) << "File descriptors not sent using KSandbox::IO::sendFileDescriptors(), msg size was" << size;
    }
    cmsg = CMSG_FIRSTHDR(&msg);
    if (msg.msg_flags & MSG_CTRUNC) {
        qCWarning(KSANDBOX) << "Message control data truncated:" << msg.msg_controllen;
    }
    if (msg.msg_flags & MSG_TRUNC) {
        qCWarning(KSANDBOX) << "Message data truncated!";
    }
    if (!cmsg) {
        qCWarning(KSANDBOX) << "No valid file descriptors sent using KSandbox::IO::sendFileDescriptors()" << msg.msg_controllen;
        return result;
    }
    KSANDBOX_REQUIRE(cmsg->cmsg_type == SCM_RIGHTS, cmsg->cmsg_type);
    KSANDBOX_REQUIRE(cmsg->cmsg_level == SOL_SOCKET, cmsg->cmsg_level);
    auto numFds = (cmsg->cmsg_len - CMSG_LEN(0)) / sizeof(int);
    // qCDebug(KSANDBOX) << "Received buffer for" << numFds << "file descriptors";
    for(uint i = 0; i < numFds; i++) {
        int fd = ((int *)CMSG_DATA(cmsg))[i];
        if (fd != -1) {
            result.push_back(fd);
        }
    }
    // qCDebug(KSANDBOX) << "Received" << result.size() << "valid file descriptors";
    return result;
}

KSandbox::FileDescriptor KSandbox::IO::receiveFileDescriptor(SocketDescriptor socket)
{
    auto result = receiveFileDescriptors(socket);
    if (result.empty()) {
         throw std::runtime_error("No file descriptors received!");
    }
    if (result.size() > 1) {
        qCWarning(KSANDBOX) << "Received" << result.size() << "valid file descriptors but only expected one!";
    }
    return FileDescriptor{result[0]};
}




void KSandbox::setProcessName(const char* name)
{
#ifdef Q_OS_FREEBSD
    setproctitle("-%s", name);
#else
    // TODO: PR_SET_MM_ARG_START/END?
    // TODO: override argv instead, PR_SET_NAME is limited to 16 chars
    // strlcpy(argv[0], title, &argv[argc-1][strlen(argv[argc-1])] - argv[0]);
    prctl(PR_SET_NAME, name);
#endif
}

