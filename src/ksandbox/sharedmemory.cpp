/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sharedmemory.h"

#include "util.h"

#include <QDebug>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <qplatformdefs.h>

#ifdef SYS_memfd_create
#include <linux/memfd.h> // for MFD_CLOEXEC
#endif


using namespace KSandbox;

SharedMemory::SharedMemory(size_t size, Flags flags, const char* debugName) {
    int syscallFlags = 0;
#ifdef Q_OS_LINUX
# if defined(SYS_memfd_create)
# pragma message("SHM using memfd_create")
    if (!(flags & Flags::DontCloseOnExec)) {
        syscallFlags |= MFD_CLOEXEC;
    }
    KSANDBOX_SYSCALL(fd = syscall(SYS_memfd_create, debugName, syscallFlags));
# elif defined(O_TMPFILE)
# pragma message("SHM using open(O_TMPFILE)")
    if (!(flags & Flags::DontCloseOnExec)) {
        syscallFlags |= O_CLOEXEC;
    }
    // TODO: is /dev/shm always a tmpfs? -> deprecate
    KSANDBOX_SYSCALL(fd = ::open("/dev/shm", O_EXCL | O_TMPFILE | O_RDWR | syscallFlags, 0600));
# else
# error "Linux kernel version is too old: supports neither memfd_create nor O_TMPFILE"
# endif
#elif defined(SHM_ANON)
#pragma message("SHM using shm_open(SHM_ANON)")
    if (!(flags & Flags::DontCloseOnExec)) {
        syscallFlags |= O_CLOEXEC;
    }
    KSANDBOX_SYSCALL(fd = ::shm_open(SHM_ANON, O_EXCL | O_CREAT | O_RDWR | syscallFlags, 0600));
#else
#error "SHM_ANON flag is not supported (FreeBSD version too old?)"
#endif
    if (size > 0) {
        // no need to resize if size is zero
        resize(size);
    }
    // qDebug("opened SHM fd %d in %p", fd, this);
}

SharedMemory::SharedMemory(FileDescriptor file) : fd(file.fd) {
    KSANDBOX_REQUIRE(fd >= 0, "Creating shared memory from invalid fd");
    // TODO: check that it is actually a valid shm fd
}

void SharedMemory::map(ssize_t size, SharedMemory::AccessMode accessMode) {
    KSANDBOX_REQUIRE(size < 0 || size > 0, size); // < 0 -> map all
    KSANDBOX_REQUIRE(fd >= 0, fd);
    KSANDBOX_REQUIRE(buf == nullptr, "SHM buffer is already mapped", buf);

    size_t available = totalSize();
    if (size < 0) {
        size = available;
    }
    KSANDBOX_REQUIRE(available >= (size_t)size, "Attempting to map more shared memory than available", available, size);

    int mode = PROT_NONE;
    if (accessMode & AccessMode::Read) {
        mode |= PROT_READ;
    }
    if (accessMode & AccessMode::Write) {
        mode |= PROT_WRITE;
    }
    KSANDBOX_SYSCALL(buf = (uchar*)mmap(nullptr, size, mode, MAP_SHARED, fd, 0), DBG_VAR(size), DBG_VAR(mode), DBG_VAR(fd));
    bufsize = size;
}

// TODO: support linux mremap()?
void SharedMemory::resize(size_t size) {
    KSANDBOX_REQUIRE(fd >= 0, fd);
    KSANDBOX_REQUIRE(bufsize <= size, "Attempting to resize shared memory to less than is currently mapped!"
        " Call unmap() first.", bufsize, size);
    KSANDBOX_SYSCALL(::ftruncate(fd, size), "Failed to resize shared memory", DBG_VAR(fd), DBG_VAR(size));
}

void SharedMemory::unmap() {
    KSANDBOX_REQUIRE_NONNULL(buf, "Attempting to unmap a null shm buffer");
    KSANDBOX_SYSCALL_NOEXCEPT(::munmap(buf, bufsize));
    buf = nullptr;
}

void SharedMemory::close() {
    KSANDBOX_REQUIRE(fd >= 0, fd, "Attempting to close invalid fd");
    // Don't use KSANDBOX_SYSCALL() here because close() should not be repeated on EINTR.
    // qDebug("closing SHM fd %d in %p", fd, this);
    if (::close(fd) == -1) {
        qWarning("close failed on %d: %s", fd, strerror(errno));
    }
    fd = -1;
}

void SharedMemory::sync() {
    KSANDBOX_REQUIRE_NONNULL(buf, "Attempting to msync a null shm buffer");
    KSANDBOX_SYSCALL_NOEXCEPT(::msync(buf, bufsize, MS_SYNC));
}

size_t SharedMemory::totalSize() const {
    // this should be safe to call on an invalid object
    if (fd == -1) {
        return 0;
    }
    QT_STATBUF statbuf;
    KSANDBOX_SYSCALL(QT_FSTAT(fd, &statbuf));
    return statbuf.st_size;
}

// std::pair<int, kj::ArrayPtr<uchar>> SharedMemory::release() {
//     auto ret = std::make_pair(fd, buffer());
//     fd = -1;
//     buf = nullptr;
//     bufsize = 0;
//     return ret;
// }

SharedMemory::SharedMemory(SharedMemory&& other) : buf(other.buf), bufsize(other.bufsize), fd(other.fd)
{
    // qDebug("Move constructing SHM %p from %p", this, &other);
    other.fd = -1;
    other.buf = nullptr;
    other.bufsize = 0;
}

SharedMemory::~SharedMemory() {
    if (buf) {
        sync();
        unmap();
    }
    if (fd >= 0) {
        close();
    }
}


