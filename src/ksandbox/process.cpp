/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "process.h"
#include "config.h"
#include "util.h"
#include "debug_p.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/prctl.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#if HAVE_SYS_PROCDESC_H
#include <sys/procdesc.h>
#else
// use the version from capsicum-test
#include "procdesc.h"
#endif

namespace KSandbox {



KSandbox::Process KSandbox::Process::fork(std::function<int()> code, Flags flags)
{
    Q_UNUSED(flags)
    // TODO: actually use them
    // this should kill receiver when we exit!
    // TODO: only do this if SurviveParentExit is not set
    // TODO: use pdfork() on FreeBSD to ensure process is killed
    int rpcSockets[2];
    int fdSockets[2];
    KSANDBOX_SYSCALL(socketpair(AF_LOCAL, SOCK_STREAM, 0, rpcSockets));
    KSANDBOX_SYSCALL(socketpair(AF_LOCAL, SOCK_STREAM, 0, fdSockets));
    int procdesc = -1;
    pid_t pid = -1;
    // Allow testing for behaviour differences between procdesc and normal
    /* static */ const bool noProcdesc = qEnvironmentVariableIsSet("KSANDBOX_NO_PROCDESC");
    if (noProcdesc) {
        qCDebug(KSANDBOX, "pdfork() explicitly disabled!");
        pid = ::fork();
    } else {
        pid = pdfork(&procdesc, PD_CLOEXEC);
        if (pid == -1 && errno == ENOSYS) {
            qCDebug(KSANDBOX, "pdfork() not supported, falling back to fork()!");
            procdesc = -1;
            pid = ::fork();
        }
    }
    if (pid == 0) {
        // qDebug("child: rpc=%d, fdt=%d, parent sockets:r=%d,f=%d", rpcSockets[1], fdSockets[1], rpcSockets[0], fdSockets[0]);
#ifdef Q_OS_LINUX
        // child process will receive a SIGTERM when the parent dies
        // keep the pdfork semantics and make sure the forked process is killed on parent exit!
        // TODO: does should we only set this if procdesc is disabled? Probably does no harm so leave it in
        prctl(PR_SET_PDEATHSIG, SIGTERM);
        // TODO should this be SIGKILL instead?
#else
        // TODO: is there anything similar on FreeBSD? probably not needed since
        // we will always use pdfork
#endif
        // child
        close(rpcSockets[0]);
        close(fdSockets[0]);
        // these env vars are read by self() after an exec()
        qputenv(SANDBOXED_PROCESS_ENV_VAR, QByteArrayLiteral("1"));
        qputenv(RPC_SOCKET_ENV_VAR, QByteArray::number(rpcSockets[1]));
        qputenv(FD_TRANSFER_SOCKET_ENV_VAR, QByteArray::number(fdSockets[1]));
        Process& p = selfImpl(false);
        // Process::self() was called already in the original process
        // -> it contains a copy of the parent process memory -> we need to update the information
        p.m_pid = getpid();
        p.m_rpcSocket = rpcSockets[1];
        p.m_fdTransferSocket = fdSockets[1];
        p.m_procdesc = -1;
        p.m_valid = true;
        // qDebug("child after (pd)fork() + init: getpid()=%d, pid=%d, p.pid()=%d, p.m_pid=%d, p.m_rpcSocket=%d, p.m_fdTransferSocket=%d", getpid(), pid, p.pid(), p.m_pid, p.m_rpcSocket, p.m_fdTransferSocket);

        int ret = code(); // now run the actual code and then exit with that return value
        ::exit(ret);
    } else if (pid >= 0) {
        // parent
        // qDebug("parent: rpc=%d, fdt=%d, child sockets:r=%d,f=%d, child pid = %d, procdesc = %d", rpcSockets[0], fdSockets[0], rpcSockets[1], fdSockets[1], pid, procdesc);
        close(rpcSockets[1]);
        close(fdSockets[1]);
        Process proc;
        proc.m_rpcSocket = rpcSockets[0];
        proc.m_fdTransferSocket = fdSockets[0];
        proc.m_pid = pid;
        proc.m_procdesc = procdesc;
        proc.m_valid = true;
        proc.m_ownsProcess = true;
        return proc;
    } else {
        KSANDBOX_FAILED_SYSCALL("fork", errno);
    }
}

Process Process::spawn(QStringList command, std::function<void()> preExec, Flags flags)
{
    return fork([&]() {
        std::vector<char*> args;
        foreach (const QString& s, command) {
            // memory leaks but we exec or exit straight afterwards this so it doesn't matter
            args.push_back(qstrdup(s.toUtf8().data()));
        }
        args.push_back(nullptr); // add the sentinel null value
        if (preExec) {
            preExec(); // do setup of file descriptors, etc.
        }
        KSANDBOX_SYSCALL(execvp(args[0], args.data()));
        return 1;
    }, flags);
}

Process::Process(Process&& other)
    : m_rpcSocket(other.m_rpcSocket)
    , m_fdTransferSocket(other.m_fdTransferSocket)
    , m_pid(other.m_pid)
    , m_procdesc(other.m_procdesc)
    , m_valid(other.m_valid)
    , m_ownsProcess(other.m_ownsProcess)
{
    other.m_rpcSocket = -1;
    other.m_fdTransferSocket = -1;
    other.m_pid = -1;
    other.m_procdesc = -1;
    other.m_valid = false;
    other.m_ownsProcess = false;
}

Process& Process::operator=(Process&& other)
{
    if (&other != this) {
        std::swap(m_fdTransferSocket, other.m_fdTransferSocket);
        std::swap(m_pid, other.m_pid);
        std::swap(m_procdesc, other.m_procdesc);
        std::swap(m_rpcSocket, other.m_rpcSocket);
        std::swap(m_valid, other.m_valid);
        std::swap(m_ownsProcess, other.m_ownsProcess);
    }
    return *this;
}

Process::~Process()
{
    if (m_valid) {
        pid_t pid = getpid();
        if (m_pid == pid) {
            qCDebug(KSANDBOX, "Destroying KSandbox::Process for self(), sockets=%d,%d", m_fdTransferSocket, m_rpcSocket);
        } else {
            qCDebug(KSANDBOX, "Closing child process: %d, sockets=%d,%d", m_pid, m_fdTransferSocket, m_rpcSocket);
        }
        // passing -1 to close() should be fine, it will just return EBADF
        ::close(m_fdTransferSocket);
        ::close(m_rpcSocket);
        // if we own the other process (i.e. not self())
        // TODO: allow spawning daemon processes that survive parent exit
        if (m_procdesc != -1) {
            Q_ASSERT(m_ownsProcess);
            // if there is a valid process descriptor we own this process
            qCDebug(KSANDBOX, "Closing process descriptor %d for PID %d from %d", m_procdesc, m_pid, pid);
            ::close(m_procdesc);
        } else if (m_ownsProcess && isAlive()) {
            Q_ASSERT(m_procdesc == -1);
            qCDebug(KSANDBOX, "Child process %d is still alive, sending SIGTERM from %d", m_pid, pid);
            ::kill(m_pid, SIGTERM); // TODO: SIGKILL instead?
        }
    }
}

Process& Process::selfImpl(bool init)
{
    // init is needed to skip the string parsing calling this after a fork() without an exec()
    static Process proc = [init]() {
        Process p;
        if (init) {
            p.m_pid = ::getpid();
            p.m_valid = true;
            p.m_rpcSocket = -1;
            p.m_fdTransferSocket = -1;
            if (qEnvironmentVariableIsSet(SANDBOXED_PROCESS_ENV_VAR)) {
                bool ok = false;
                p.m_rpcSocket = qEnvironmentVariableIntValue(RPC_SOCKET_ENV_VAR, &ok);
                if (!ok || !KSandbox::IO::isSocketFd(p.m_rpcSocket)) {
                    qCDebug(KSANDBOX, "PID %d RPC socket is invalid: fd=%d (env='%s')", getpid(), p.m_rpcSocket, getenv(RPC_SOCKET_ENV_VAR));
                    p.m_rpcSocket = -1;
                    p.m_valid = false;
                }
                p.m_fdTransferSocket = qEnvironmentVariableIntValue(FD_TRANSFER_SOCKET_ENV_VAR, &ok);
                if (!ok || !KSandbox::IO::isSocketFd(p.m_fdTransferSocket)) {
                    qCDebug(KSANDBOX, "PID %d FD transfer socket is invalid: fd=%d (env='%s')", getpid(), p.m_fdTransferSocket, getenv(FD_TRANSFER_SOCKET_ENV_VAR));
                    p.m_fdTransferSocket = -1;
                    p.m_valid = false;
                }
            }
        }
        return p;
    }();
    return proc;
}

bool Process::isAlive() const
{
    if (m_procdesc >= 0) {
        return pdkill(m_procdesc, 0) == 0;
    } else {
        return kill(m_pid, 0) == 0;
    }
}

bool Process::wait(int* status, struct rusage* usage)
{
    if (!isValid()) {
        qCWarning(KSANDBOX, "Attempting to wait for default constructed KSandbox::Process");
        return false;
    }
    if (!m_ownsProcess) {
        qCWarning(KSANDBOX, "Attempting to wait for non-owned process %d from %d", m_pid, getpid());
        return false;
    }
    int ret;
    if (m_procdesc >= 0) {
        KSANDBOX_SYSCALL(ret = pdwait4(m_procdesc, status, 0, usage));
        // do I need to call close() after pdwait?
        m_procdesc = -1;
        // child has exited -> no need to kill it in the
    } else {
        qCDebug(KSANDBOX, "Waiting for KSandbox::Process without pdfork() support");
        KSANDBOX_SYSCALL(ret = wait4(m_pid, status, 0, usage));
        // child has exited -> no need to kill it in the constructor
        m_ownsProcess = false;
    }
    return true; // ret >= 0; but that is always the case since we used KSANDBOX_SYSCALL
}



}
