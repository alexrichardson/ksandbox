/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "ksandbox_export.h"

#include <QByteArray>
#include <QDebug>
#include <sys/types.h>
#include <stdexcept>
#include <functional>
#include <system_error>

#define SYSCALL_RETRY_ON_EINTR(call, onError) \
    __extension__ ({ \
        auto __syscall_result = call; \
        while (__syscall_result == ((decltype(__syscall_result))-1) && errno == EINTR) {\
            __syscall_result = call;\
        }\
        if ((intptr_t)__syscall_result < 0) {\
            onError \
        }\
        __syscall_result;\
    })
#define KSANDBOX_FAILED_SYSCALL(callStr, error, ...) \
    throw KSandbox::makeException(KSandbox::SyscallException(QLatin1String(callStr), error), ##__VA_ARGS__);
#define KSANDBOX_SYSCALL(call, ...) \
    SYSCALL_RETRY_ON_EINTR(call, KSANDBOX_FAILED_SYSCALL(#call, errno, ##__VA_ARGS__))
#define KSANDBOX_SYSCALL_NOEXCEPT(call) \
    SYSCALL_RETRY_ON_EINTR(call, ;)
#define KSANDBOX_SYSCALL_MESSAGE_ON_ERROR(dbgOutput, call, ...) \
    SYSCALL_RETRY_ON_EINTR(call, dbgOutput << KSandbox::makeException(KSandbox::SyscallException(QLatin1String(#call), errno),##__VA_ARGS__).message();)
#define DBG_VAR(var) " " #var " = ", var

#define KSANDBOX_REQUIRE2(condition, conditionStr, ...) do { if (!(condition)) { throw KSandbox::makeException(KSandbox::RequirementFailed( \
    QLatin1String("Requirement \"" conditionStr "\" failed.")),##__VA_ARGS__); } } while(false)

#define KSANDBOX_REQUIRE(condition, ...) KSANDBOX_REQUIRE2(condition, #condition, ##__VA_ARGS__)
#define KSANDBOX_REQUIRE_NONNULL(condition, ...) KSANDBOX_REQUIRE2((condition) != nullptr, #condition " != nullptr",##__VA_ARGS__)
#define KSANDBOX_REQUIRE_EQUAL(a, b, ...)  KSANDBOX_REQUIRE2((a) == (b), #a " == " #b, DBG_VAR(a), DBG_VAR(b), ##__VA_ARGS__)


namespace KSandbox {

    class KSANDBOX_EXPORT Exception : public std::exception {
    public:
        explicit Exception(const QString& str = {});
        explicit Exception(QLatin1String str);
        virtual ~Exception();
        const char* what() const noexcept override {
            // this may throw in OOM situations, but calling std::terminate then should be fine
            asUtf8 = msg.toUtf8();
            return asUtf8.constData();
        }
        template<typename T> Exception& operator<<(const T& val) {
            dbg.nospace() << val;
            return *this;
        }
        QDebug& debug() { return dbg; }
        QString message() const { return msg; }
    protected:
        QString msg;
        mutable QByteArray asUtf8;
        QDebug dbg;
    };

    class KSANDBOX_EXPORT SyscallException : public Exception {
    public:
        SyscallException(QLatin1String call, int error);
        virtual ~SyscallException();
        std::error_code error() const { return code; }
    private:
        std::error_code code;
    };

    class KSANDBOX_EXPORT RequirementFailed : public Exception {
    public:
        using Exception::Exception;
        virtual ~RequirementFailed();
    };

    template<typename E>
    inline E makeException(E&& e) {
        return std::move(e);
    }
    template<typename E, typename T, typename... Args>
    inline E makeException(E&& e, const T& var, Args&&... args) {
        e << var;
        return makeException(std::move(e), std::forward<Args>(args)...);
    }



    struct FileDescriptor {
        explicit FileDescriptor(int fd = -1) : fd(fd) {}
        FileDescriptor& operator=(int fd) { this->fd = fd; return *this; }
        int fd;
    };

    struct SocketDescriptor {
        explicit SocketDescriptor(int fd = -1) : fd(fd) {}
        SocketDescriptor& operator=(int fd) { this->fd = fd; return *this; }
        operator FileDescriptor() { return FileDescriptor{fd}; }
        int fd;
    };

    namespace IO {
        KSANDBOX_EXPORT bool isSocketFd(int fd);
        /**
         * @return @c true if @p fd is a currenty open fd
         * @warning This is racy in a multithreaded environment since it may have been reused
         * It should only be used in code where you can guarantee that no fds are being created.
         */
        KSANDBOX_EXPORT bool isValidFd(FileDescriptor fd);
        inline bool isSocketFd(FileDescriptor sock) { return isSocketFd(sock.fd); }
        KSANDBOX_EXPORT void setNonblocking(int fd, bool nonblocking = true);
        KSANDBOX_EXPORT void setCloseOnExec(int fd);
        KSANDBOX_EXPORT void closefrom(int fromFd);
        /** @return the directory where the current executable is
        * @note require /compat/linux/proc on FreeBSD
        * @note doesn't work in capability mode due to reading global files
        */
        KSANDBOX_EXPORT QByteArray executableDir();

        KSANDBOX_EXPORT ssize_t sendFileDescriptors(SocketDescriptor socket, const std::vector<int>& fds);

        inline ssize_t sendFileDescriptor(SocketDescriptor socket, int fd) {
            return sendFileDescriptors(socket, { fd });
        }
        inline ssize_t sendFileDescriptor(SocketDescriptor socket, FileDescriptor f) {
            return sendFileDescriptor(socket, f.fd);
        }

        /** @return the file descriptor that was passed over the socket (discarding the data) or @c -1 on failure */
        KSANDBOX_EXPORT FileDescriptor receiveFileDescriptor(SocketDescriptor socket);

        /** @return the file descriptors that were passed over the socket (discarding the data) or an empty vector on failure */
        KSANDBOX_EXPORT std::vector<int> receiveFileDescriptors(SocketDescriptor socket, uint maxFds = 10);
    }

    struct ExitStatus {
        pid_t pid = 0;
        int exitCode = -1;
        int signal = -1;
        bool exitedNormally = false;
        bool signalled = false;
    };


    /**
     * @note On Linux this uses the getrandom() syscall (works in sandbox), on FreeBSD arc4random() (might not work in sandbox)
     * @return a random number usable for cryptographic purposes
     */
    KSANDBOX_EXPORT uint32_t secureRandomNumber();
    // TODO add uint32_t secureRandomNumber(uint32_t maximum?); like arc4random_uniform()
    /**
     * @note On Linux this uses the getrandom() syscall (works in sandbox), on FreeBSD arc4random() (might not work in sandbox)
     * @warning For large buffers it may be a better idea to seed a secure random number generator with a secureRandomNumber()
     * since the call might block for a long time.
     * @return a buffer filled with data usable for cryptographic purposes
     */
    KSANDBOX_EXPORT QByteArray secureRandomBuffer(size_t size);



    KSANDBOX_EXPORT ExitStatus waitForExit(pid_t pid);

    /**
     * Sets the name of the process (currently limited to 16 characters on linux.
     * TODO: overwrite /proc/pid/commandline + PR_SET_NAME
     * @note The current Linux implementation is limited to 16 characters
     */
    KSANDBOX_EXPORT void setProcessName(const char* name);

    KSANDBOX_EXPORT void enterCapabilityMode();
}
