/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "ksandbox_export.h"
#include "util.h"

#include <QtGlobal>

namespace KSandbox {

// TODO: make this class movable

/**
 * Corresponds to an @b anonymous shared memory object.
 * QSharedMemory or shm_open() create a globally visible (and accessible!) file in /dev/shm so
 * we don't want to use that since arbitrary processes could change the e.g. the rendered pixmaps.
 *
 * On Linux this uses memfd_create() (requires Kernel 3.17) or falls back to
 * using open("/dev/shm", O_TMPFILE) for Kernel 3.11 or newer. Older Kernels are not supported.
 *
 * On FreeBSD this is done by passing SHM_ANON to shm_open()
 *
 * @note Other non-sandboxed processes can change the size of this mapping meaning any access to buffer()
 * could cause a SIGBUS signal. You should make sure that all untrusted processes receive a file descriptor
 * to this shared memory object that does not permit changing the size. Additionally - depending on the
 * sandboxing requirements - it is probably a good idea to ensure that the file descriptor has been set to read-only.
 *
 */
class KSANDBOX_EXPORT SharedMemory
{
    Q_DISABLE_COPY(SharedMemory)
public:
    enum Flags {
        DefaultFlags, ///< default shared memory flags (close on exec)
        DontCloseOnExec ///< keep this shared memory file descriptor open even after calling execve()
    };
    /** Executable purposfully omitted here */
    enum AccessMode {
        Read = 0x1,
        Write = 0x2,
        ReadWrite = Read | Write
    };
    enum Rights {
        AllowWrite = 0x1,
        AllowRead = 0x2, // can't remove this right on Linux
        AllowTruncate = 0x4,
    };

    /**
     * Creates a new anonymous (unmapped) shared memory object.
     * @param size the size of the shared memory in bytes
     * @param flags the flags to pass to the kernel when creating the shared memory. By default the shared memory file
     * descriptor will be closed on exec, SharedMemory::Flags::DontCloseOnExec can be passed to change this behaviour.
     * @param debugName On Linux >= 3.17 this name will be visible in /proc/self/fd for debugging purposes
     */
    explicit SharedMemory(size_t size = 0, Flags flags = DefaultFlags, const char* debugName = "");

    /**
     * Create a (unmapped) SharedMemory object from an existing file descriptor.
     * @param fd must be a valid anonymous shared memory object file descriptor
     */
    explicit SharedMemory(FileDescriptor fd);
    // make sure file descriptors aren't implicitly converted to size_t
    SharedMemory(int) = delete;
    // allow move construction
    SharedMemory(SharedMemory&& other);
    ~SharedMemory();

    void resize(size_t newSize);
    /**
     * Maps the shared memory into the current processes address space
     * @param size the number of bytes to map or -1 to map all available memory
     * @param accessMode the access to the memory that should be granted
     */
    void map(ssize_t size = -1, KSandbox::SharedMemory::AccessMode accessMode = ReadWrite);
    void unmap();
    /** ensure that all other processes see the updates that have been made */
    void sync();
    void close();

    /**
     * Limit the access rights to this shared memory segment. It probably makes sense to remove the @c AllowTruncate
     * right before sending the file descriptor to another process since that could otherwise cause SIGBUS errors
     * by shrinking the memory segment while we still assume the full size.
     *
     * @warning On Linux without Capsicum these rights apply to the underlying file and @b not the file descriptor
     * I.e. if another reference to this shared memory segment was created
     */
    void removeRights(Rights rights);

    size_t size() const { return bufsize; }
    const uchar* cbegin() const { return buf; }
    const uchar* cend() const { return buf + bufsize; }
    const uchar* begin() const { return cbegin(); }
    const uchar* end() const { return cend(); }
    uchar* begin() { return buf; }
    uchar* end() { return buf + bufsize; }

    /** @return the underlying file descriptor */
    FileDescriptor handle() { return FileDescriptor{fd}; }
    size_t mappedSize() const { return bufsize; }
    bool isMapped() const { return buf != nullptr; }
    /**
     * @return The current size of this shared memory object (this is not necessarily the same as mappedSize()).
     * It can be changed by other processes (depending on their capabilities).
     */
    size_t totalSize() const;

private:
    uchar* buf = nullptr;
    size_t bufsize = 0;
    int fd = -1;
};

}
