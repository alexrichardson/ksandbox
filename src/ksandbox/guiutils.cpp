/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "guiutils.h"
#include "util.h"
#include <unistd.h>

#include <QImage>


struct ShmInfo {
    KSandbox::SharedMemory shm;
};

static void releaseSHM(void* handle) {
    auto shm = static_cast<KSandbox::SharedMemory*>(handle);
    // qDebug("Deleting SHM %p", shm);
    delete shm;
}

QImage KSandbox::imageFromSharedMemory(KSandbox::SharedMemory&& shm, size_t width, size_t height, QImage::Format format)
{
    auto ptr = std::unique_ptr<KSandbox::SharedMemory>(new KSandbox::SharedMemory{std::move(shm)});
    return imageFromSharedMemory(std::move(ptr), width, height, format);
}

QImage KSandbox::imageFromSharedMemory(std::unique_ptr<KSandbox::SharedMemory> mem, size_t width, size_t height, QImage::Format format)
{
    // If the shared memory has not been mapped yet do it now
    if (!mem->isMapped()) {
        // qDebug("mapping SHM automatically");
        mem->map();
    }
    KSANDBOX_REQUIRE_NONNULL(mem->begin());
    auto ptr = mem.get();
    // qDebug("QImage(%p, %zu, %zu, %d, %p, %p)", ptr->buffer().begin(), width, height, format, releaseSHM, ptr);
    QImage img(ptr->begin(), width, height, format, releaseSHM, mem.release());
    // qDebug("taken ownership over shm (%p)!", ptr);
    Q_ASSERT(!img.isNull());
    // if the mapped size is not bigger than the byte count we could get crashes when reading from the image later
    KSANDBOX_REQUIRE(ptr->mappedSize() >= (size_t)img.byteCount(), ptr->mappedSize(), img.byteCount(), ptr->totalSize());
    return img;
}


