/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2015  Alex Richardson <arichardson.kde@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include "ksandbox_export.h"
#include "util.h"


#include <QtGlobal>
#include <QStringList>

#include <functional>

#include <signal.h>

class QSocketNotifier;
struct rusage;

namespace KSandbox {

class KSANDBOX_EXPORT Process {
    Q_DISABLE_COPY(Process)
public:
    Process() = default;
    Process(Process&&);
    Process& operator=(Process&&);
    ~Process();

    enum class ProcessFlags {
        None = 0,
        SurviveParentExit = 1,

        Defaults = None
    };
    Q_DECLARE_FLAGS(Flags, ProcessFlags)

    static Process fork(std::function<int()> code, Flags flags = ProcessFlags::Defaults);
    static Process spawn(QStringList command, std::function<void()> preExec = nullptr, Flags flags = ProcessFlags::Defaults);
    /** @return the current Process
     * @note only pid is guaranteed to be set, all other members might be invalid!
     */
    static Process& self() { return selfImpl(true); }

    inline SocketDescriptor rpcSocket() const { return SocketDescriptor(m_rpcSocket); }
    inline SocketDescriptor fdTransferSocket() const { return SocketDescriptor(m_fdTransferSocket); }
    inline pid_t pid() const { return m_pid; }
    inline int processDescriptor() const { return m_procdesc; }

    bool wait(int* status, rusage* usage = nullptr); // TODO: better API
    bool isAlive() const;
    /** @return whether this is a valid process or was created using the default constructor */
    bool isValid() const { return m_valid; }

private:
    static Process& selfImpl(bool init);


    int m_rpcSocket = -1;
    int m_fdTransferSocket = -1;
    pid_t m_pid = -1;
    int m_procdesc = -1;
    bool m_valid = false;
    bool m_ownsProcess = false;
};

}

Q_DECLARE_OPERATORS_FOR_FLAGS(KSandbox::Process::Flags)
